﻿#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using GDLibrary.Managers;
using GDLibrary;
using GDLibrary.Debug;
using GDLibrary.Utility;
#endregion

/*
 * Bugs:
 * -------
 * - Per-pixel Collision Detection Collision Response (CDCR) bug with animated spritesheet
 * - ScreenManager bounds not updating correctly on camera move - causing sprites to be culled unexpectedly
 * - Camera scale affects does not accurately affect mouse position - causing problems during design phase 
 * 
 * To Do:
 * -------
 * - Post processing - complete scene manager
 * - Split screen - debug BSP and re-test
 * - Add font and effect dictionary
 * - PhysicsUtility - simple projectile
 * 
 * Done/Fixed:
 * -------
 * 
 * Version 3 (Week 11)
 * - Added scene manager (starting point for post processing in V4) (See Draw() in all DrawableGameComponents)
 * - Split screen (BSP bug)
 * - Main::Initialize() - moved some methods based on temporal dependencies (e.g. loadDebug() etc)
 * 
 * Version 2 (Week 10)
 * - Level loader from text file or image
 * 
 * Version 1 (Week 9)
 * - Starter code (managers, menu, content)
 * 
 */



namespace GDApp
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Main : Game
    {
        //graphics
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //managers
        private ScreenManager screenManager;
        private KeyboardManager keyboardManager;
        private ObjectManager objectManager;
        private CameraManager cameraManager;
        private MouseManager mouseManager;
        private SoundManager soundManager;
        private MenuManager menuManager;
        private BSPLayoutManager bspLayoutManager;

        //fonts
        private SpriteFont hudFont;

        //dictionaries
        private TextureDictionary textureDictionary;
        private SceneManager sceneManager;


        #region Properties
        /*
         * Notice that the manager properties (e.g. ScreenManager formerly used by MenuManager) 
         * have been removed - why? because we now access through IServiceContainer.
         */

        public Camera2D ActiveCamera
        {
            get
            {
                return cameraManager.ActiveCamera;
            }
        }
        public GraphicsDeviceManager Graphics
        {
            get
            {
                return graphics;
            }
        }
        public SpriteBatch SpriteBatch
        {
            get
            {
                return spriteBatch;
            }
        }
        #endregion


        public Main()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {

            loadDictionaries();

            loadFonts();
            loadTextures();

            loadManagers(ScreenResolutionType.SVGA, 50);

            loadSounds();

            loadGameObjectsFromFile();
            loadGameObjects();

            loadCameras();

            loadBSP("bsp.xml", "debugrectangle");

            base.Initialize();
        }

        private void loadGameObjectsFromFile()
        {
            LevelLoader loader = new LevelLoader();

            //level 1
            this.objectManager.Add(loader.Load(this.textureDictionary["lvl"].Texture, 48, 48));
            //level 3
            this.objectManager.Add(loader.Load(this.textureDictionary["level3"].Texture, 48, 48));
        }

        private void loadBSP(string strName, string strTexture)
        {
            BSPManager bspManager = new BSPManager(strName);
            IServiceContainer.AddService(typeof(BSPManager), bspManager);
            //transforms need to check manager to see if the corresponding bounds is inside one of the sectors on the list
            Transform2D.bspManager = bspManager;

            //use this code only when designing the BSP
            Texture2D texture = this.textureDictionary["debugrectangle"].Texture;
            this.bspLayoutManager = new BSPLayoutManager(this, texture, this.hudFont, bspManager, true);
            IServiceContainer.AddService(typeof(BSPLayoutManager), bspLayoutManager);
            Components.Add(bspLayoutManager);
        }

        private void loadCameras()
        {
            Vector2 cameraPosition
                = new Vector2(this.GraphicsDevice.Viewport.Width / 2, this.GraphicsDevice.Viewport.Height / 2);
            CameraTransform2D cameraTransform2D = new CameraTransform2D(cameraPosition, 0, Vector2.One, this.GraphicsDevice.Viewport);

            Viewport leftViewPort = graphics.GraphicsDevice.Viewport;
            //leftViewPort.Width /= 2;

            cameraManager.Add(new MoveableCamera("player L",
                cameraTransform2D, leftViewPort, KeyData.CAMERA_MOVE_KEYS_B));

            //Viewport rightViewPort = leftViewPort;
            //rightViewPort.X = leftViewPort.Width;

            //CameraTransform2D cloneTransform2D
            //            = (CameraTransform2D)cameraTransform2D.Clone();
            //cameraManager.Add(new MoveableCamera("player R",
            //    cloneTransform2D, rightViewPort, KeyData.CAMERA_MOVE_KEYS_A));
        }

        private void loadDebug()
        {
            IServiceContainer.AddService(typeof(DebugDrawer),
                new DebugDrawer(this, textureDictionary["debugrectangle"].Texture, Color.Red));
        }
        private void loadFPS()
        {
            IServiceContainer.AddService(typeof(PerformanceDebug), new PerformanceDebug(this, hudFont, Color.White));
        }

        private void loadManagers(ScreenResolutionType resolutionType, int playDelayInMS)
        {
            this.screenManager = new ScreenManager(this, resolutionType, 50);
            Components.Add(screenManager); //needs to be updated
            IServiceContainer.AddService(typeof(ScreenManager), screenManager);

            this.soundManager = new SoundManager(this, playDelayInMS);
            IServiceContainer.AddService(typeof(SoundManager), soundManager);
            Components.Add(this.soundManager);

            this.cameraManager = new CameraManager(this);
            Components.Add(cameraManager); //needs to be updated
            IServiceContainer.AddService(typeof(CameraManager), cameraManager);

            this.keyboardManager = new KeyboardManager(this);
            Components.Add(keyboardManager); //needs to be updated
            IServiceContainer.AddService(typeof(KeyboardManager), keyboardManager);

            loadCameras(); //See loadManagers()

            this.mouseManager = new MouseManager(this, true);
            Components.Add(mouseManager); //needs to be updated
            IServiceContainer.AddService(typeof(MouseManager), mouseManager);

            this.objectManager = new ObjectManager(this);
            IServiceContainer.AddService(typeof(ObjectManager), objectManager);

            string[] menuTextureStrArray = { "Images\\Menu\\mainmenu", "Images\\Menu\\volumemenu" };
            this.menuManager = new MenuManager(this, menuTextureStrArray, hudFont, 20 * Integer2.One);
            IServiceContainer.AddService(typeof(MenuManager), menuManager);

            //scene manager uses debug so must load here
            loadDebug();
            loadFPS();

            this.sceneManager = new SceneManager(this);
            Components.Add(sceneManager);
            IServiceContainer.AddService(typeof(SceneManager), sceneManager);

        }
        private void loadDictionaries()
        {
            //simply a repository of references
            this.textureDictionary = new TextureDictionary(this);
            IServiceContainer.AddService(typeof(TextureDictionary), textureDictionary);
        }

        private void loadGameObjects()
        {
            #region Load From File
            //to do...
            #endregion
            TextureData tData = null;
            Transform2D transform = null;
            GraphicComponent graphicsComponent = null;

            tData = textureDictionary["bullet"];
            transform = new Transform2D(-Vector2.UnitY, Vector2.One, 0, Vector2.One, new Vector2(tData.Texture.Width / 2, tData.Texture.Height / 2), tData.Dimensions);
            graphicsComponent = new GraphicComponent(tData, Color.White, 1, SpriteEffects.None);
            Bullet b = new Bullet(transform, graphicsComponent, Vector2.One);
            IServiceContainer.AddService(typeof(Bullet), b);

            tData = textureDictionary["p"];
            transform = new Transform2D(-Vector2.UnitY, new Vector2(200, 200), 0, Vector2.One, new Vector2(tData.Texture.Width / 2, tData.Texture.Height / 2), tData.Dimensions);
            graphicsComponent = new GraphicComponent(tData, Color.White, 1, SpriteEffects.None);
            Player p = new Player(transform, graphicsComponent);
            objectManager.Add(p);
        }

        private void loadSounds()
        {
            soundManager.Add(new SoundEffectData("Audio\\Events\\birth", 0.5f, -1, 0.5f, false));
        }
        private void loadFonts()
        {
            //to do - create a FontManager similar to TextureManager
            this.hudFont = Content.Load<SpriteFont>(@"Fonts\\hud");
        }
        private void loadTextures()
        {
            //debug
            textureDictionary.Add(new TextureData(this, "Images\\Debug\\debugrectangle"));

            //character
            textureDictionary.Add(new TextureData(this, "Images\\Characters\\p"));
            textureDictionary.Add(new TextureData(this, "Images\\Characters\\robot"));

            textureDictionary.Add(new TextureData(this, "Images\\Weapons\\bullet"));

            //level
            textureDictionary.Add(new TextureData(this, "Images\\Level\\lvl"));
            textureDictionary.Add(new TextureData(this, "Images\\Level\\level2"));
            textureDictionary.Add(new TextureData(this, "Images\\Level\\level3"));
            //decorator
            textureDictionary.Add(new TextureData(this, "Images\\Decorator\\white_border"));
            textureDictionary.Add(new TextureData(this, "Images\\Decorator\\white_square"));
            textureDictionary.Add(new TextureData(this, "Images\\Decorator\\white_edge_south"));
            textureDictionary.Add(new TextureData(this, "Images\\Decorator\\antenna"));
            textureDictionary.Add(new TextureData(this, "Images\\Decorator\\circle"));
            textureDictionary.Add(new TextureData(this, "Images\\Decorator\\wave"));
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            //unload update list and dispose of objects
            this.objectManager.Dispose();

            //free all texture objects
            this.textureDictionary.Dispose();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
