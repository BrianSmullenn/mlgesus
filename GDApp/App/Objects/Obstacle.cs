﻿using System;
using GDApp;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    class Obstacle : DrawableObject
    {

        public Obstacle(Transform2D transform, GraphicComponent graphicsComponent)
            : base(transform, graphicsComponent)
        {
        }
    }
}
