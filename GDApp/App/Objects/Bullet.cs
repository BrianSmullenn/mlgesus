﻿using GDLibrary.Collision;
using GDLibrary.Managers;
using GDLibrary.Utility;
using Microsoft.Xna.Framework;
using GDApp;
using System;
namespace GDLibrary
{
    public class Bullet : DrawableObject, IService
    {

        #region Variables
        Vector2 direction;
        #endregion

        #region Properties
        public Vector2 Direction
        {
            get
            {
                return this.direction;
            }
            set
            {
                this.direction = value;
            }
        }
        #endregion


        public Bullet(Transform2D transform, GraphicComponent graphicsComponent)
            : base(transform, graphicsComponent)
        {
        }

        public Bullet(Transform2D transform, GraphicComponent graphicsComponent, Vector2 direction)
            : base(transform, graphicsComponent)
        {
            this.direction = direction;
        }

        public override void Update(GameTime gameTime)
        {
            handleInput(gameTime);

            this.Transform.applyInput(gameTime);

            //applyOtherCollisionConstraints(gameTime);
            //applyCollisionConstraints(gameTime);
            base.Update(gameTime);
        }

        public override void handleInput(GameTime gameTime)
        {
            this.Transform.InputMove = Vector2.Normalize(this.direction) *gameTime.ElapsedGameTime.Milliseconds * AppData.BULLET_MOVE_SPEED;
        }


        public override void applyOtherCollisionConstraints(GameTime gameTime)
        {
            if (!this.ScreenManager.Bounds.Contains(this.Transform.Bounds))
            {
                this.ObjectManager.Remove(this);
            }
            base.applyOtherCollisionConstraints(gameTime);
        }

        public override void applyCollisionConstraints(GameTime gameTime)
        {
            for (int i = 0; i < this.ObjectManager.DrawList.Count; i++)
            {
                GameObject o = this.ObjectManager.DrawList[i];

                if (o is Obstacle)
                {
                    Obstacle collidee = (Obstacle)o;

                    Vector2 cdcrVector = CollisionUtility.IntersectsNonAA(this, collidee);

                    if (cdcrVector != CollisionUtility.COLLISION_FAIL)
                    {
                        this.ObjectManager.Remove(this);
                    }
                }
            }
        }


        public new object Clone()
        {
            Transform2D clonedTransform = (Transform2D)this.Transform.Clone();
            GraphicComponent clonedGraphicsComponent = (GraphicComponent)this.GraphicsComponent.Clone();

            return new Bullet(clonedTransform, clonedGraphicsComponent);
        }
    }
}
