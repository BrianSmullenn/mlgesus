﻿using GDApp;
using GDLibrary.Collision;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public class Player : DrawableObject
    {
        #region Variables
        private float spinSpeed = 1;
        private bool fired = true;
        private Bullet bullet;
        private bool falling = false;
        #endregion

        #region Properties

        #endregion

        public Player(Transform2D transform, GraphicComponent graphicsComponent)
            :base(transform, graphicsComponent)
        {
            this.bullet = (Bullet)IServiceContainer.GetService(typeof(Bullet));
            
        }

        public override void Update(GameTime gameTime)
        {
            handleMovement(gameTime);
            spinShoot();
            iZWallzzColliDoScopezzz();

            base.Update(gameTime);
        }

        // probably should add a bit of acceleration to movement aswell
        private void handleMovement(GameTime gameTime)
        {
            if (this.KeyboardManager.IsKeyDown(KeyData.MOVE_LEFT))
            {
                this.Transform.Position += -AppData.SHIP_MOVE_SPEED * Vector2.UnitX * gameTime.ElapsedGameTime.Milliseconds / 10.0f;
            }
            if (this.KeyboardManager.IsKeyDown(KeyData.MOVE_RIGHT))
            {
                this.Transform.Position += AppData.SHIP_MOVE_SPEED * Vector2.UnitX * gameTime.ElapsedGameTime.Milliseconds / 10.0f;
            }
            if (this.KeyboardManager.IsKeyDown(KeyData.MOVE_DOWN))
            {
                this.Transform.Position += AppData.SHIP_MOVE_SPEED * Vector2.UnitY * gameTime.ElapsedGameTime.Milliseconds / 10.0f;
            }
            if (this.KeyboardManager.IsKeyDown(KeyData.MOVE_UP))
            {
                this.Transform.Position += -AppData.SHIP_MOVE_SPEED * Vector2.UnitY * gameTime.ElapsedGameTime.Milliseconds / 10.0f;
            }
        }

        //private void handleInputInTheEventThatCollisionWasDetected_NowYouCantMoveAsNormal()
        //{
        //    if (this.KeyboardManager.IsKeyDown(KeyData.MOVE_LEFT) && this.Transform.X)
        //    {
        //        this.Transform.Position += -AppData.SHIP_MOVE_SPEED * Vector2.UnitX * gameTime.ElapsedGameTime.Milliseconds / 10.0f;
        //    }
        //}

        private void spinShoot()
        {
            if (this.KeyboardManager.IsKeyDown(KeyData.BLAZE))
            {
                this.Transform.Rotation += MathHelper.ToRadians(1) * spinSpeed;
                spinSpeed += 0.1f;
                fired = false;
            }

            if (this.KeyboardManager.IsKeyUp(KeyData.BLAZE) && fired == false)
            {
                fired = true;
                spinSpeed = 1;
                Bullet b = (Bullet)this.bullet.Clone();
                b.Transform.Position = this.Transform.Position; // bullet speed is somehow speeding up
                b.Direction = this.Transform.Look;
                this.ObjectManager.Add(b);
            }
        }

        private void scopezzzzzzz()
        {

        }

        public void datCollizionzzSimba(GameObject collidee)
        {
            //base.applyCollisionConstraints(gameTime);
            float collideeRightSide = collidee.Transform.Position.X+collidee.Transform.Bounds.Width*2;
            if(this.Transform.Position.X - this.GraphicsComponent.Texture.Width/2 < collideeRightSide)
            {
                this.Transform.Position = new Vector2(collideeRightSide + 0.w1f,this.Transform.Position.Y);
            }
        }

        private void iZWallzzColliDoScopezzz()
        {
            bool onPlatform = false; //nmcg

            for (int i = 0; i < this.ObjectManager.DrawList.Count; i++)
            {
                GameObject o = this.ObjectManager.DrawList[i];

                if ((o is Obstacle) && !onPlatform) //nmcg - if im not already on a platform then allow me to test for cd against platforms
                {
                    Obstacle collidee = (Obstacle)o;

                    //bool collides = CollisionUtility.Intersects(this.Transform.Bounds, collidee.Transform.Bounds);

                    Vector2 cdcrvector = CollisionUtility.IntersectsNonAA(this, collidee);

                    if (cdcrvector != CollisionUtility.COLLISION_FAIL)
                    //if(this.Transform.Bounds.Intersects(collidee.Transform.Bounds))
                    {
                        this.GraphicsComponent.Color = Color.Red;
                        //falling = false;
                        onPlatform = true; //nmcg
                        //this.GraphicsComponent.Color = Color.
                        datCollizionzzSimba(collidee);
                        //return true;
                    }
                    else
                    {
                        this.GraphicsComponent.Color = Color.White;
                        //this means not colliding and apply gravity
                        //falling = true;
                        //return false;
                    }      
                }
            }
            //return false;
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }

        public new object Clone()
        {
            Transform2D clonedTransform = (Transform2D)this.Transform.Clone();
            GraphicComponent clonedGraphicsComponent = (GraphicComponent)this.GraphicsComponent.Clone();
            return new Player(clonedTransform, clonedGraphicsComponent);
        }

        public override void Dispose()
        {
            //call base
            base.Dispose();
        }
    }
}
