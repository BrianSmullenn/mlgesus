﻿using GDLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GDApp
{
    public class MoveableCamera : MoveableCamera2D
    {
        public MoveableCamera(string name, CameraTransform2D transform, Viewport viewPort, Keys[] moveKeys)
            : base(name, transform, viewPort, moveKeys)
        {

        }

        public override void Update(GameTime gameTime)
        {
            //test for input
            handleInput(gameTime);

            //e.g. does the camera need to stay within a particular rectangle?
            //applyAllOtherConstraints();

            base.Update(gameTime);
        }

        public override void handleInput(GameTime gameTime)
        {
            float moveSpeed = CameraData.MOVEABLE_CAMERA_SPEED * gameTime.ElapsedGameTime.Milliseconds / 10.0f;

            if (this.KeyboardManager.IsKeyDown(this.MoveKeys[KeyData.MOVEABLE_CAMERA_KEYSINDEX_SLOW_MODIFIER]))
            {
                moveSpeed *= CameraData.MOVEABLE_CAMERA_SPEED_SLOW_MODIFIER;
            }
            if (this.KeyboardManager.IsKeyDown(this.MoveKeys[KeyData.MOVEABLE_CAMERA_KEYSINDEX_FAST_MODIFIER]))
            {
                moveSpeed *= CameraData.MOVEABLE_CAMERA_SPEED_FAST_MODIFIER;
            }

            //up/down
            if (this.KeyboardManager.IsKeyDown(this.MoveKeys[KeyData.MOVEABLE_CAMERA_KEYSINDEX_UP]))
            {
                this.Transform.InputMove = -Vector2.UnitY * moveSpeed;  //Up
            }
            else if (this.KeyboardManager.IsKeyDown(this.MoveKeys[KeyData.MOVEABLE_CAMERA_KEYSINDEX_DOWN]))
            {
                this.Transform.InputMove = Vector2.UnitY * moveSpeed;   //Down
            }

            //left/right
            if (this.KeyboardManager.IsKeyDown(this.MoveKeys[KeyData.MOVEABLE_CAMERA_KEYSINDEX_LEFT]))
            {
                this.Transform.InputMove = -Vector2.UnitX * moveSpeed;  //Left
            }
            else if (this.KeyboardManager.IsKeyDown(this.MoveKeys[KeyData.MOVEABLE_CAMERA_KEYSINDEX_RIGHT]))
            {
                this.Transform.InputMove = Vector2.UnitX * moveSpeed;   //Right
            }

            //bugs
            //if (this.KeyboardManager.IsKeyDown(this.MoveKeys[KeyData.MOVEABLE_CAMERA_KEYSINDEX_ZOOM_IN]))
            //{
            //    this.Transform.InputScale = CameraData.MOVEABLE_CAMERA_ZOOM_IN_FACTOR * gameTime.ElapsedGameTime.Milliseconds / 10.0f;           //Zoom In
            //}
            //else if (this.KeyboardManager.IsKeyDown(this.MoveKeys[KeyData.MOVEABLE_CAMERA_KEYSINDEX_ZOOM_OUT]))
            //{
            //    this.Transform.InputScale = CameraData.MOVEABLE_CAMERA_ZOOM_OUT_FACTOR * gameTime.ElapsedGameTime.Milliseconds / 10.0f;           //Zoom Out
            //}

            //we can apply a reset immediately as its normally only done in exceptional circumstances
            if (this.KeyboardManager.IsKeyDown(this.MoveKeys[KeyData.MOVEABLE_CAMERA_KEYSINDEX_RESET]))
            {
                this.Transform.reset();
            }


        }
    }
}
