﻿using GDLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GDApp
{
    public class TargetCamera : Camera2D
    {
        private GameObject gameObject;

        public TargetCamera(string name, CameraTransform2D transform, 
            Viewport viewPort, GameObject gameObject)
            : base(name, transform, viewPort)
        {
            this.gameObject = gameObject;
        }

        public override void Update(GameTime gameTime)
        {
            applyTargetMovement();

            base.Update(gameTime);
        }

        private void applyTargetMovement()
        {
            if(this.gameObject != null)
                this.Transform.Position = this.gameObject.Transform.Position;
        }
    }
}
