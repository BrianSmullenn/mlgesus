﻿using Microsoft.Xna.Framework.Input;

namespace GDApp
{
    //sealed == final in Java == cannot be inherited from
    public sealed class CameraData
    {
        #region Camera
        //readonly == final == cannot be changed
        public static readonly float MOVEABLE_CAMERA_SPEED = 4f;
        public static readonly float MOVEABLE_CAMERA_SPEED_SLOW_MODIFIER = 0.1f;
        public static readonly float MOVEABLE_CAMERA_SPEED_FAST_MODIFIER = 2f;

        public static readonly float MOVEABLE_CAMERA_ZOOM_IN_FACTOR = 0.6875f; //10% zoom in based on 60Hz
        public static readonly float MOVEABLE_CAMERA_ZOOM_OUT_FACTOR = 0.5625f; //90% zoom out based on 60Hz

        #endregion
    }
}
