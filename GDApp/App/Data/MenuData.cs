﻿using System;
using Microsoft.Xna.Framework;


namespace GDApp
{
    public class MenuData
    {
        #region MENU_STRINGS;
        //all the strings shown to the user through the menu
        public static String GAME_TITLE = "Space Invaders";
        public static String MENU_RESUME = "Play/Resume";
        public static String MENU_SAVE = "Save";
        public static String MENU_AUDIO = "Audio";
        public static String MENU_EXIT = "Exit";

        public static String MENU_VOLUMEUP = "Volume Up";
        public static String MENU_VOLUMEDOWN = "Volume Down";
        public static String MENU_BACK = "Back";

        public static Color MENU_INACTIVE_COLOR = Color.White;
        public static Color MENU_ACTIVE_COLOR = Color.Red;

        #endregion;

    }
}
