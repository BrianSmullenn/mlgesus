﻿
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
namespace GDApp
{
    //sealed == final in Java == cannot be inherited from
    public sealed class AppData
    {
        public static readonly float ONE_DEGREE_IN_RADIANS = MathHelper.ToRadians(1);
        public static readonly int POSITION_SCALE_ROUND_PRECISION = 1;
        public static readonly int ROTATION_ROUND_PRECISION = 3;

        #region Camera
        //readonly == final == cannot be changed
        public static readonly float MOVEABLE_CAMERA_SPEED = 4f;
        public static readonly float MOVEABLE_CAMERA_ZOOM_IN_FACTOR = 0.6875f; //10% zoom in based on 60Hz
        public static readonly float MOVEABLE_CAMERA_ZOOM_OUT_FACTOR = 0.5625f; //90% zoom out based on 60Hz

        public static readonly int MOVEABLE_CAMERA_KEYSINDEX_UP  = 0;
        public static readonly int MOVEABLE_CAMERA_KEYSINDEX_DOWN = 1;
        public static readonly int MOVEABLE_CAMERA_KEYSINDEX_LEFT = 2;
        public static readonly int MOVEABLE_CAMERA_KEYSINDEX_RIGHT = 3;
        public static readonly int MOVEABLE_CAMERA_KEYSINDEX_ZOOM_IN = 4;
        public static readonly int MOVEABLE_CAMERA_KEYSINDEX_ZOOM_OUT = 5;
        public static readonly int MOVEABLE_CAMERA_KEYSINDEX_RESET = 6;
        #endregion


        #region Ship
        public static readonly float SHIP_MOVE_SPEED = 2f;
        public static readonly float BULLET_MOVE_SPEED = 1f;
        public static readonly float BULLET_FIRE_RATE_IN_UPDATES = 30;
        public static readonly float TWENTY_DEGREE_ROTATION_IN_RADIANS = MathHelper.ToRadians(10);
        #endregion

        #region Serialization
        public static readonly string SCREEN_LAYOUT_XML_FILE = "screenlayout.xml";
        public static Color COLOR_LEVEL_LOADER_IGNORE = new Color(0,0,0,0);
        #endregion



    }
}
