﻿using Microsoft.Xna.Framework.Input;

namespace GDApp
{
    public sealed class KeyData
    {

        #region Camera
        public static readonly Keys[] CAMERA_MOVE_KEYS
            = { 
                  Keys.NumPad8, Keys.NumPad2, 
                  Keys.NumPad4, Keys.NumPad6, 
                  Keys.NumPad7, Keys.NumPad9, 
                  Keys.NumPad5, 
                  Keys.LeftShift, Keys.LeftControl
              };

        public static Keys[] CAMERA_MOVE_KEYS_A = CAMERA_MOVE_KEYS;

        public static Keys[] CAMERA_MOVE_KEYS_B 
            = { 
                  Keys.T, Keys.G, //UD
                  Keys.F, Keys.H, //LR
                  Keys.R, Keys.Y, //zoom
                  Keys.U, 
                  Keys.RightShift, Keys.RightControl
              };



        public static readonly int MOVEABLE_CAMERA_KEYSINDEX_UP  = 0;
        public static readonly int MOVEABLE_CAMERA_KEYSINDEX_DOWN = 1;
        public static readonly int MOVEABLE_CAMERA_KEYSINDEX_LEFT = 2;
        public static readonly int MOVEABLE_CAMERA_KEYSINDEX_RIGHT = 3;
        public static readonly int MOVEABLE_CAMERA_KEYSINDEX_ZOOM_IN = 4;
        public static readonly int MOVEABLE_CAMERA_KEYSINDEX_ZOOM_OUT = 5;
        public static readonly int MOVEABLE_CAMERA_KEYSINDEX_RESET = 6;
        public static readonly int MOVEABLE_CAMERA_KEYSINDEX_SLOW_MODIFIER = 7;
        public static readonly int MOVEABLE_CAMERA_KEYSINDEX_FAST_MODIFIER = 8;
        #endregion

        #region Screen Level Manager
        public static readonly Keys KEY_SCREEN_LAYOUT_MANAGER_ADD_SCREEN = Keys.F1;
        public static readonly Keys KEY_SCREEN_LAYOUT_MANAGER_REMOVE_SCREEN = Keys.F4;

        public static readonly Keys KEY_SCREEN_LAYOUT_MANAGER_ADD_DRAWABLEOBJECT = Keys.F5;
        public static readonly Keys KEY_SCREEN_LAYOUT_MANAGER_REMOVE_DRAWABLEOBJECT = Keys.F8;
        #endregion

        #region Menu
        public static Keys KEY_PAUSE_SHOW_MENU = Keys.P;
        #endregion

        #region Player
        public static readonly Keys BLAZE = Keys.Space;
        public static readonly Keys MOVE_LEFT = Keys.A;
        public static readonly Keys MOVE_RIGHT = Keys.D;
        public static readonly Keys MOVE_UP = Keys.W;
        public static readonly Keys MOVE_DOWN = Keys.S;
        #endregion

        #region BSP
        public static Keys KEY_LAYOUT_MANAGER_UP = Keys.I;
        public static Keys KEY_LAYOUT_MANAGER_DOWN = Keys.K;
        public static Keys KEY_LAYOUT_MANAGER_LEFT = Keys.J;
        public static Keys KEY_LAYOUT_MANAGER_RIGHT = Keys.L;
        public static Keys KEY_LAYOUT_MANAGER_FINE_REPOSITION = Keys.F5;
        public static Keys KEY_LAYOUT_MANAGER_COARSE_RESIZE = Keys.F6;
        public static Keys KEY_LAYOUT_MANAGER_FINE_RESIZE = Keys.F7;


        #endregion
    }
}
