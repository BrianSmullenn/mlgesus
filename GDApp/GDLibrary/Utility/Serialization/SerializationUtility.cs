﻿using Microsoft.Xna.Framework.Storage;
using System.IO;
using System.Xml.Serialization;

namespace GDLibrary
{
    public class SerializationUtility
    {
        //See http://msdn.microsoft.com/en-us/library/ms172873.aspx
        public static void Save<T>(T obj, string fileName, FileMode fileMode)
        {
            //open the file
            FileStream fStream = File.Open(fileName, fileMode);
            //convert to XML
            XmlSerializer xmlSerial = new XmlSerializer(typeof(T));
            //write to stream
            xmlSerial.Serialize(fStream, obj);
            //close the stream
            fStream.Close();
        }

        //See http://msdn.microsoft.com/en-us/library/ms172872.aspx
        public static T Load<T>(string fileName, FileAccess fileAccess)
        {
            if (File.Exists(fileName))
            {
                //open for read
                FileStream fStream = File.Open(fileName, FileMode.Open, fileAccess);

                //read the data from file
                XmlSerializer xmlSerial = new XmlSerializer(typeof(T));
                T data = (T)xmlSerial.Deserialize(fStream);

                //housekeeping
                fStream.Close();
                return data;
            }

            return default(T); //doesnt exist
        }

        public static void Delete(string fileName)
        {
            if (File.Exists(fileName))
                File.Delete(fileName);
        }
    }
}
