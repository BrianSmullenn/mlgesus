﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Utility
{
    public class GeometryUtility
    {
        //renamed to a more generalised Add() from ProjectRectangle()
        public static Rectangle Add(Rectangle rectangle, Vector2 moveVector)
        {
            Rectangle projectedRectangle = rectangle;
            projectedRectangle.X += (int)Math.Round(moveVector.X);
            projectedRectangle.Y += (int)Math.Round(moveVector.Y);
            return projectedRectangle;
        }

        //note the use of the ref keyword, this allows us to modify the original rectangle and not have to make a copy - as in ProjectRectangle above
        public static void Add(ref Rectangle rectangle, Vector2 addVector)
        {
            rectangle.X += (int)Math.Round(addVector.X);
            rectangle.Y += (int)Math.Round(addVector.Y);
        }

        public static Rectangle Clone(Rectangle rectangle)
        {
            return new Rectangle(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
        }

        public static Matrix Clone(Matrix matrix)
        {
            return new Matrix(matrix.M11, matrix.M12, matrix.M13, matrix.M14,
                                matrix.M21, matrix.M22, matrix.M23, matrix.M24,
                                    matrix.M31, matrix.M32, matrix.M33, matrix.M34,
                                    matrix.M41, matrix.M42, matrix.M43, matrix.M44);
        }



        public static float Distance(Vector2 vector, Point point)
        {
            return Vector2.Distance(vector, new Vector2(point.X, point.Y));
        }
    }
}
