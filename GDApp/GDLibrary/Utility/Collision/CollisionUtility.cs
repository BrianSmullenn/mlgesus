﻿using GDLibrary.Utility;
using Microsoft.Xna.Framework;
using System;

namespace GDLibrary.Collision
{
    public class CollisionUtility
    {
        protected static float alphaThreshold = 10;

        public static MOVEABLE_CAMERA_KEYS IsVerticallyContained(Rectangle container, Rectangle containee)
        {
            return IsVerticallyContained(container, containee, 0);
        }

        public static MOVEABLE_CAMERA_KEYS IsVerticallyContained(Rectangle container, Rectangle containee, Vector2 moveVector)
        {
            return IsVerticallyContained(container, containee, moveVector, 0);
        }

        public static MOVEABLE_CAMERA_KEYS IsVerticallyContained(Rectangle container, Rectangle containee, int padding)
        {
            if (containee.Top < container.Top + padding)
            {
                return MOVEABLE_CAMERA_KEYS.NotContainedTop;
            }
            else if (containee.Bottom > container.Bottom - padding)
            {
                return MOVEABLE_CAMERA_KEYS.NotContainedBottom;
            }

            return MOVEABLE_CAMERA_KEYS.Contains;
        }

        public static MOVEABLE_CAMERA_KEYS IsVerticallyContained(Rectangle container, Rectangle containee, Vector2 moveVector, int padding)
        {
            return IsVerticallyContained(container, GeometryUtility.Add(containee, moveVector), padding);
        }



        public static MOVEABLE_CAMERA_KEYS IsHorizontallyContained(Rectangle container, Rectangle containee)
        {
            return IsHorizontallyContained(container, containee, 0);
        }

        public static MOVEABLE_CAMERA_KEYS IsHorizontallyContained(Rectangle container, Rectangle containee, Vector2 moveVector)
        {
            return IsHorizontallyContained(container, containee, moveVector, 0);
        }

        public static MOVEABLE_CAMERA_KEYS IsHorizontallyContained(Rectangle container, Rectangle containee, int padding)
        {
            if (containee.Left < container.Left + padding)
            {
                return MOVEABLE_CAMERA_KEYS.NotContainedLeft;
            }
            else if (containee.Right > container.Right - padding)
            {
                return MOVEABLE_CAMERA_KEYS.NotContainedRight;
            }

            return MOVEABLE_CAMERA_KEYS.Contains;
        }

        public static MOVEABLE_CAMERA_KEYS IsHorizontallyContained(Rectangle container, Rectangle containee, Vector2 moveVector, int padding)
        {
            return IsHorizontallyContained(container, GeometryUtility.Add(containee, moveVector), padding);
        }

        public static bool Intersects(Rectangle collidee, Rectangle collider)
        {
            return collider.Intersects(collidee);
        }

       
        public static bool Intersects(Rectangle collidee, Rectangle collider, Vector2 moveVector)
        {
            Rectangle projectedCollider = GeometryUtility.Add(collider, moveVector);
            return projectedCollider.Intersects(collidee);
        }

        /*
         * This method should no longer be used since bounding rectangles will not always be axis-aligned.
         * Instead use either a simple BoundsA.Intersects(BoundsB) intersection test or IntersectsNonAA() for per-pixel tests
         */
        /*
        public static bool IntersectsAA(Color[] textureColorDataA, Color[] textureColorDataB,
                                      Rectangle boundsRectangleA, Rectangle boundsRectangleB)
        {
            // Find the extents of the rectangle intersection
            int top = Math.Max(boundsRectangleA.Top, boundsRectangleB.Top);
            int bottom = Math.Min(boundsRectangleA.Bottom, boundsRectangleB.Bottom);
            int left = Math.Max(boundsRectangleA.Left, boundsRectangleB.Left);
            int right = Math.Min(boundsRectangleA.Right, boundsRectangleB.Right);

            // Check every point within the intersection bounds
            for (int i = top; i < bottom; i++)
            {
                for (int j = left; j < right; j++)
                {
                    // Get the color of both pixels at this point
                    Color colorA = textureColorDataA[(j - boundsRectangleA.Left) +
                                         (i - boundsRectangleA.Top) * boundsRectangleA.Width];
                    Color colorB = textureColorDataB[(j - boundsRectangleB.Left) +
                                         (i - boundsRectangleB.Top) * boundsRectangleB.Width];

                    // If both pixels are not completely transparent,
                    if (colorA.A >= alphaThreshold && colorB.A >= alphaThreshold)
                    {
                        // then an intersection has been found
                        return true;
                    }
                }
            }

            // No intersection found
            return false;
        }
        */

        public static Rectangle CalculateTransformedBoundingRectangle(Rectangle rectangle,
                                                           Matrix transform)
        {
            //   Matrix inverseMatrix = Matrix.Invert(transform);
            // Get all four corners in local space
            Vector2 leftTop = new Vector2(rectangle.Left, rectangle.Top);
            Vector2 rightTop = new Vector2(rectangle.Right, rectangle.Top);
            Vector2 leftBottom = new Vector2(rectangle.Left, rectangle.Bottom);
            Vector2 rightBottom = new Vector2(rectangle.Right, rectangle.Bottom);

            // Transform all four corners into work space
            Vector2.Transform(ref leftTop, ref transform, out leftTop);
            Vector2.Transform(ref rightTop, ref transform, out rightTop);
            Vector2.Transform(ref leftBottom, ref transform, out leftBottom);
            Vector2.Transform(ref rightBottom, ref transform, out rightBottom);

            // Find the minimum and maximum extents of the rectangle in world space
            Vector2 min = Vector2.Min(Vector2.Min(leftTop, rightTop),
                                      Vector2.Min(leftBottom, rightBottom));
            Vector2 max = Vector2.Max(Vector2.Max(leftTop, rightTop),
                                      Vector2.Max(leftBottom, rightBottom));

            // Return that as a rectangle
            return new Rectangle((int)Math.Ceiling(min.X), (int)Math.Ceiling(min.Y),
                                 (int)Math.Ceiling(max.X - min.X), (int)Math.Ceiling(max.Y - min.Y));
        }

        public static Vector2 COLLISION_FAIL = new Vector2(-1,-1);

        private static Vector2 pos1 = Vector2.Zero, pos2 = Vector2.Zero;
        private static int x2 = 0, y2 = 0;
        private static Color[,] colorDataA, colorDataB;
        private static Matrix mat1to2;

        public static Vector2 IntersectsNonAA(DrawableObject a, DrawableObject b)
        {
            if (a.Transform.Bounds.Intersects(b.Transform.Bounds))
            {
                mat1to2 = a.Transform.World * Matrix.Invert(b.Transform.World);
                colorDataA = a.GraphicsComponent.GetColorData();
                colorDataB = b.GraphicsComponent.GetColorData();

                //is this information constant? If so, we can move out to TextureData
                int width1 = colorDataA.GetLength(0); //bug - nmcg
                int height1 = colorDataA.GetLength(1);
                int width2 = colorDataB.GetLength(0);
                int height2 = colorDataB.GetLength(1);

                for (int x1 = 0; x1 < width1; x1++)
                {
                    for (int y1 = 0; y1 < height1; y1++)
                    {
                        pos1 = new Vector2(x1, y1);
                        pos2 = Vector2.Transform(pos1, mat1to2);
                        x2 = (int)Math.Round(pos2.X);
                        y2 = (int)Math.Round(pos2.Y);
                        if (((x2 >= 0) && (x2 < width2)) && ((y2 >= 0) && (y2 < height2)))
                        {
                            if ((colorDataA[x1, y1].A > alphaThreshold) && (colorDataB[x2, y2].A > alphaThreshold))
                            {
                                return Vector2.Transform(pos1, a.Transform.World);
                            }
                        }
                    }
                } //Non AA CDCR
            }

            return COLLISION_FAIL;      
        }


        /// <summary>
        /// Used to determine if two objects occupy the same sector
        /// </summary>
        /// <param name="a">Game object</param>
        /// <param name="b">Game object</param>
        /// <returns></returns>
        public static bool IntersectsBSP(GameObject a, GameObject b)
        {
            return ((a.Transform.BSPSector & b.Transform.BSPSector) != 0);
        }
    }
}
