﻿using GDApp;
using GDLibrary.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace GDLibrary.Utility
{
    public class LevelLoader
    {
        private TextureDictionary textureDictionary;

        public LevelLoader()
        {
            this.textureDictionary = (TextureDictionary)IServiceContainer.GetService(typeof(TextureDictionary));
        }

        public List<GameObject> Load(Texture2D texture, int tileWidth, int tileHeight)
        {
            List<GameObject> list = new List<GameObject>();
            Color[] colorData = new Color[texture.Height * texture.Width];
            texture.GetData<Color>(colorData);

            Color c; Vector2 position; GameObject obj;

            for (int y = 0; y < texture.Height; y++)
            {
                for (int x = 0; x < texture.Width; x++)
                {
                    c = colorData[x + y * texture.Width];

                    if (!c.Equals(AppData.COLOR_LEVEL_LOADER_IGNORE))
                    {
                        position = new Vector2(x * tileWidth, y * tileHeight);
                        obj = getObjectFromColor(c, position, tileWidth, tileHeight);

                        if (obj != null)
                            list.Add(obj);
                    }
                } //end for x
            } //end for y
            return list;
        }

        //How can we improve this method?
        private GameObject getObjectFromColor(Color colorFromTexture, Vector2 position, int tileWidth, int tileHeight)
        {
            float depth = 1, rotation = 0;
            TextureData tData = null;
            Vector2 scale = Vector2.One;
            Transform2D transform = null;
            GraphicComponent graphicComponent = null;

            #region Level 1
            if (colorFromTexture.Equals(Color.White))  //white border
            {
                //set depth based on color
                //depth = X;
                tData = this.textureDictionary["white_border"];
                scale = new Vector2((float)tileWidth / tData.Width, (float)tileHeight / tData.Height);
                rotation = 0;
                transform = new Transform2D(Vector2.UnitX, position, rotation, scale, tData.Dimensions);
                graphicComponent = new GraphicComponent(tData, Color.White, depth, SpriteEffects.None);
                return new Obstacle(transform, graphicComponent);
            }
            else if (colorFromTexture.Equals(Color.Red)) //wave
            {
                tData = this.textureDictionary["white_square"];
                scale = new Vector2((float)tileWidth / tData.Width, (float)tileHeight / tData.Height);
                rotation = 0;
                transform = new Transform2D(Vector2.UnitX, position, rotation, scale, tData.Dimensions);
                graphicComponent = new GraphicComponent(tData, Color.White, depth, SpriteEffects.None);
                return new DrawableObject(transform, graphicComponent);
            }
            else if (colorFromTexture.Equals(new Color(0, 255, 0)))  //edge
            {
                tData = this.textureDictionary["white_edge_south"];
                scale = new Vector2((float)tileWidth / tData.Width, (float)tileHeight / tData.Height);
                rotation = 0;
                transform = new Transform2D(Vector2.UnitX, position, rotation, scale, tData.Dimensions);
                graphicComponent = new GraphicComponent(tData, Color.Gray, depth, SpriteEffects.FlipVertically);
                return new DrawableObject(transform, graphicComponent);
            }
            else if (colorFromTexture.Equals(new Color(255, 0, 255)))
            {
                tData = this.textureDictionary["white_edge_south"];
                scale = new Vector2((float)tileWidth / tData.Width, (float)tileHeight / tData.Height);
                rotation = 0;
                transform = new Transform2D(Vector2.UnitX, position, rotation, scale, tData.Dimensions);
                graphicComponent = new GraphicComponent(tData, Color.Gray, depth, SpriteEffects.None);
                return new DrawableObject(transform, graphicComponent);
            }
            else if (colorFromTexture.Equals(new Color(0, 0, 255))) //wave
            {
                tData = this.textureDictionary["wave"];
                scale = new Vector2((float)tileWidth / tData.Width, (float)tileHeight / tData.Height);
                rotation = 0;
                transform = new Transform2D(Vector2.UnitX, position, rotation, scale, tData.Dimensions);
                graphicComponent = new GraphicComponent(tData, Color.White, depth, SpriteEffects.None);
                return new DrawableObject(transform, graphicComponent);
            }
            else if (colorFromTexture.Equals(new Color(255, 255, 0))) //wave
            {
                tData = this.textureDictionary["antenna"];
                scale = new Vector2((float)tileWidth / tData.Width, (float)tileHeight / tData.Height);
                rotation = 0;
                transform = new Transform2D(Vector2.UnitX, position, rotation, scale, tData.Dimensions);
                graphicComponent = new GraphicComponent(tData, Color.White, depth, SpriteEffects.None);
                return new DrawableObject(transform, graphicComponent);
            }
            #endregion

            #region Level 3
            else if (colorFromTexture.Equals(new Color(128, 0, 0))) //wave
            {
                tData = this.textureDictionary["circle"];
                scale = new Vector2((float)tileWidth / tData.Width, (float)tileHeight / tData.Height);
                rotation = 0;
                transform = new Transform2D(Vector2.UnitX, position, rotation, scale, tData.Dimensions);
                graphicComponent = new GraphicComponent(tData, Color.White, depth, SpriteEffects.None);
                return new DrawableObject(transform, graphicComponent);
            }
            else if (colorFromTexture.Equals(new Color(0, 255, 255))) //wave
            {
                tData = this.textureDictionary["robot"];
                scale = new Vector2((float)tileWidth / tData.Width, (float)tileHeight / tData.Height);
                rotation = 0;
                transform = new Transform2D(Vector2.UnitX, position, rotation, scale, tData.Dimensions);
                graphicComponent = new GraphicComponent(tData, Color.White, depth, SpriteEffects.None);
                return new DrawableObject(transform, graphicComponent);
            }
            #endregion

            else if (colorFromTexture.Equals(new Color(255, 255, 0))) //wave
            {
                return null;//remember we can also return animated objects, pickup objects i.e. any object type
            }
            //add as many else ifs as you have types

            return null;
        }


    }
}
