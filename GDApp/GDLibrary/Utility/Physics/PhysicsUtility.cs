﻿using Microsoft.Xna.Framework;
namespace GDLibrary.Utility
{
    public class PhysicsUtility
    {
        public static float TimeScaleFactor = 4096;
        public static Vector2 Gravity = -9.8f * Vector2.UnitY;
        public static Vector2 Wind = 4f * Vector2.UnitX; 

        /// <summary>
        /// Simple linear motion with constant acceleration due to gravity
        /// </summary>
        /// <param name="initialPosition"></param>
        /// <param name="initialVelocity"></param>
        /// <param name="totalElapsedTimeInMs"></param>
        /// <param name="gravityAcceleration"></param>
        /// <param name="otherAcceleration"></param>
        /// <returns></returns>
        public static Vector2 GetSimpleProjectileDisplacement(Vector2 initialPosition, Vector2 initialVelocity,
          float totalElapsedTimeInMs, Vector2 gravityAcceleration, Vector2 otherAcceleration)
        {
            return Vector2.Zero; //to do       
        }

        /// <summary>
        /// Simple linear motion with constant acceleration due to gravity AND another user-defined force
        /// </summary>
        /// <param name="initialPosition"></param>
        /// <param name="initialVelocity"></param>
        /// <param name="totalElapsedTimeInMs"></param>
        /// <param name="gravityAcceleration"></param>
        /// <returns></returns>
        public static Vector2 GetSimpleProjectileDisplacement(Vector2 initialPosition, Vector2 initialVelocity,
         float totalElapsedTimeInMs, Vector2 gravityAcceleration)
        {
            return Vector2.Zero; //to do       
        }
    }
}
