﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace GDLibrary.Utility
{
    public class TextureUtility
    {
 
        public static Color[] Get1DColorData(Texture2D texture)
        {
            //read data into 1d array
            Color[] colorData = new Color[texture.Width * texture.Height];
            texture.GetData(colorData);
            return colorData;
        }
        public static Color[,] Get2DColorData(Texture2D texture)
        {
            int width = texture.Width;
            int height = texture.Height;

            //read data into 1d array
            Color[] colors1D = new Color[width * height];
            texture.GetData(colors1D);

            //create 2d array to store data
            Color[,] colorData = new Color[width, height];

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    colorData[x, y] = colors1D[x + y * width];
                }
            }

            return colorData;
        }

        //converts texture into a list of 2D color[,] array objects - used for animation
        public static List<Color[,]> setSourceColorDataList(Texture2D texture, int frameWidth, int frameHeight, int numberOfFrames)
        {
            List<Color[,]> sourceColorDataList = new List<Color[,]>(numberOfFrames);

            int width = texture.Width;
            int height = texture.Height;

            //read data into 1d array
            Color[] colors1D = new Color[width * height]; 
            texture.GetData(colors1D);

            //create 2d array to store data
            Color[,] colors2D;

            //read each frame into a seperate colors2D array and add it to the list
            //then when we want to now the color data for a particular frame we just query the list
            for (int i = 0; i < numberOfFrames; i++)
            {
                colors2D = new Color[frameHeight, frameWidth]; //bug fix

                for (int row = 0; row < frameHeight; row++)
                {
                    for (int col = 0; col < frameWidth; col++)
                    {
                        //bug fix
                        colors2D[row, col] = colors1D[col + row * frameWidth * numberOfFrames + (i * frameWidth)];
                    }
                }
                sourceColorDataList.Add(colors2D);
            }

            return sourceColorDataList;
        }

     
    }
}
