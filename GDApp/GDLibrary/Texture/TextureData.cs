﻿using GDLibrary.Utility;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GDApp;

namespace GDLibrary
{
    public class TextureData
    {
        #region Variables
        private string name, path;

        private Texture2D texture;
        private Color[,] colorData;
        private Integer2 dimensions;
        #endregion

        #region Properties
    
        public Color[,] ColorData
        {
            get
            {
                if (colorData == null)
                {
                    colorData = TextureUtility.Get2DColorData(this.texture);
                }
                return colorData;
            }
        }
        public string Name
        {
            get
            {
                return name;
            }
            
        }
        public Texture2D Texture
        {
            get
            {
                return texture;
            }
            set
            {
                texture = value;
            }
        }
        public Integer2 Dimensions
        {
            get
            {
                if(dimensions == null)
                {
                    dimensions = new Integer2(texture.Width, texture.Height);
                }
                return dimensions;
            }
            set
            {
                dimensions = value;
            }
        }
        public int Width
        {
            get
            {
                return texture.Width;
            }
        }
        public int Height
        {
            get
            {
                return texture.Height;
            }
        }
        #endregion

        public TextureData(Main game, string path)
        {
            this.path = path;
            this.name = StringUtility.ParseNameFromPath(path).ToLower();
            this.texture = game.Content.Load<Texture2D>(@"" + this.path);
        }

        public virtual void Dispose()
        { 
            this.texture.Dispose();
            this.colorData = null;
            this.name = null;
            this.path = null;
            this.dimensions = null;
        }
    }
}
















