﻿using GDLibrary.Utility;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using GDApp;

namespace GDLibrary
{
    public class AnimatedTextureData : TextureData
    {
        #region Variables
        //this is a list containing all the source rectangle color data
        protected List<Color[,]> sourceColorDataList;

        //width and height of a single frame inside the animation
        protected int frameWidth, frameHeight, totalNumberOfFrames;
        #endregion

        #region Properties
        
        //get the color data used for per-pixel intersects() method on frame index specified
        public Color[,] this[int frameIndex]
        {
            get
            {
                return sourceColorDataList[frameIndex];
            }
        }
        public List<Color[,]> ColorDataList
        {
            get
            {
                return sourceColorDataList;
            }
        }
        public new Integer2 Dimensions
        {
            get
            {
                return new Integer2(frameWidth, frameHeight);
            }
        }
        public new int Width
        {
            get
            {
                return frameWidth;
            }
        }
        public new int Height
        {
            get
            {
                return frameHeight;
            }
        }

        public int TotalNumberOfFrames
        {
            get
            {
                return totalNumberOfFrames;
            }
        }
        #endregion

        public AnimatedTextureData(Main game, string path, int totalNumberOfFrames)
            : base(game, path)
        {
            this.frameWidth = this.Texture.Width/totalNumberOfFrames;
            this.frameHeight = this.Texture.Height;
            this.totalNumberOfFrames = totalNumberOfFrames;
            this.sourceColorDataList = TextureUtility.setSourceColorDataList(this.Texture, frameWidth, frameHeight, totalNumberOfFrames);
        }

        public override void Dispose()
        {
            for (int i = 0; i < this.sourceColorDataList.Count; i++)
            {
                this.sourceColorDataList[i] = null; //tag each array for garbage collection(GC)
            }
            this.sourceColorDataList.Clear(); //remove from the list
            this.sourceColorDataList = null; //tag list for GC

            base.Dispose();
        }
    }
}
















