﻿using GDLibrary.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GDApp;

namespace GDLibrary.Debug
{
    public class DebugDrawer : IService
    {
        #region Variables
        private Main game;
        private Texture2D texture;
        private Color color;
        private bool bPause;
        private ObjectManager objectManager;

        //temps
        private GameObject gameObject;
        #endregion

        #region Properties
        public bool Pause
        {
            get
            {
                return bPause;
            }
            set
            {
                bPause = value;
            }
        }
        #endregion

        public DebugDrawer(Main game, Texture2D texture, Color color)
        {
            this.game = game;
            this.texture = texture;
            this.color = color;
            
            //draw by default
            this.bPause = false;

            this.objectManager = (ObjectManager)IServiceContainer.GetService(typeof(ObjectManager));
        }

        public void Draw(GameTime gameTime)
        {
            if (!bPause)
            {
                for (int i = 0; i < objectManager.DrawList.Count; i++)
                {
                    this.gameObject = objectManager.DrawList[i];
                    game.SpriteBatch.Draw(texture, gameObject.Transform.Bounds, null, color, 0, Vector2.Zero, SpriteEffects.None, 1);
                }
            }
        }
    }
}
