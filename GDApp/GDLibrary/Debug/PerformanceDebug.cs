﻿using GDLibrary;
using GDLibrary.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GDApp;
namespace GDLibrary
{
    public class PerformanceDebug : IService
    {
        private SpriteFont font;
        private Main game;

        private int fpsCounter;
        private string strFPS="";
        private int elapsedTime;
        private ObjectManager objectManager;
        private Vector2 strPosition;
        private CameraManager cameraManager;
        private Color textColor = Color.White;

        //temp vars
        private Vector2 position;

        public PerformanceDebug(Main game, SpriteFont font, Color textColor)
        {
            this.font = font;
            this.game = game;
            this.textColor = textColor;

            this.strPosition = new Vector2(game.Graphics.PreferredBackBufferWidth, 0);
            this.cameraManager = (CameraManager)IServiceContainer.GetService(typeof(CameraManager));
        }
        public void Update(GameTime gameTime)
        {
            fpsCounter++;
            elapsedTime += gameTime.ElapsedGameTime.Milliseconds;

            if (elapsedTime > 1000)
            {
                strFPS = "FPS: " + fpsCounter;
                fpsCounter = 0;
                elapsedTime = 0;
            }
        }
        public void Draw(GameTime gameTime, int drawCount)
        {
            game.SpriteBatch.DrawString(font, strFPS, strPosition + new Vector2(-250, 50), textColor);
            //Frames are dropped (i.e. isrunningslowly = true) when the update call takes too long and there is no remaining time for a draw call.
            game.SpriteBatch.DrawString(font, "Frames Dropped:" + gameTime.IsRunningSlowly, strPosition + new Vector2(-250, 70), textColor);
            game.SpriteBatch.DrawString(font, "Drawn Objects:" + drawCount, strPosition + new Vector2(-250, 90), textColor);
        }
    }
}
