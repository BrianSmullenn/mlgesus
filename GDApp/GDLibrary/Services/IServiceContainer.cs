﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;
namespace GDLibrary
{
    public static class IServiceContainer 
    {
        private static Dictionary<System.Type, IService> dictionary = new Dictionary<System.Type, IService>();

        public static IService GetService(System.Type type)
        {
            if (dictionary.ContainsKey(type))
                return dictionary[type];
            else
                return null;
        }

        public static void AddService(System.Type type, IService service)
        {
            if (!dictionary.ContainsKey(type))
            {
                dictionary.Add(type, service);
            }
        }

        public static void Clear()
        {
            dictionary.Clear();
        }

        public static int Count()
        {
            return dictionary.Count;
        }
    }
}
