﻿using System.ComponentModel;
namespace GDLibrary
{
    //See http://www.codeproject.com/Articles/13821/Adding-Descriptions-to-your-Enumerations
    //for a discussion of the use of the Description component
    public enum ScreenResolutionType : sbyte
    {
        //See ScreenManagerHelper also
        [Description("vga")]
        VGA = 0,
        [Description("svga")]
        SVGA = 1,
        [Description("wvga")]
        WVGA = 2,
        [Description("xga")]
        XGA = 3,
        [Description("hd720")]
        HD720 = 4,
        [Description("wxga")]
        WXGA = 5,
        [Description("sxga")]
        SXGA = 6,
        [Description("uxga")]
        UXGA = 7,
        [Description("hd1080")]
        HD1080 = 8,
        [Description("qxga")]
        QXGA = 9
    };
}
