﻿namespace GDLibrary
{
    public enum AnimationStateType : sbyte
    {   
        Play = 0,
        PlayRepeat = 1,
        Pause = 2,
        Finished = 3
    }
}
