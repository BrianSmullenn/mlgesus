﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GDLibrary
{
    public class Camera2D
    {
        #region Variables
        private string name;
        private CameraTransform2D transform;
        private Viewport viewPort;

        //only N cameras may be active at a time (e.g. single-player or split-screen)
        private bool isActive;
        #endregion

        #region Properties
        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }
        public string Name
        {
            get
            {
                return name;
            }
        }
        public CameraTransform2D Transform
        {
            get
            {
                return transform;
            }
            set
            {
                transform = value;
            }
        }
        public Viewport Viewport
        {
            get
            {
                return viewPort;
            }
            set
            {
                viewPort = value;
            }
        }
        #endregion

        public Camera2D(string name, CameraTransform2D transform, Viewport viewPort)
        {
            this.name = name;
            this.transform = transform;
            this.viewPort = viewPort;
        }

       public virtual void Update(GameTime gameTime)
       {
           //if no collision and no other constraints then apply input
           this.Transform.applyInput(gameTime);

           this.transform.Update();
       }

    }
}
