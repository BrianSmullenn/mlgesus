﻿using GDLibrary.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GDLibrary
{
    public class MoveableCamera2D : Camera2D
    {
        #region Variables
        private Keys[] moveKeys;
        private KeyboardManager keyboardManager;
        //private MouseManager mouseManager;
        #endregion

        #region Properties
        public Keys[] MoveKeys
        {
            get
            {
                return moveKeys;
            }
            set
            {
                moveKeys = value;
            }
        }
        protected KeyboardManager KeyboardManager
        {
            get
            {
                return keyboardManager;
            }
            set
            {
                keyboardManager = value;
            }
        }
        #endregion



        public MoveableCamera2D(string name, CameraTransform2D transform, Viewport viewPort, Keys[] moveKeys)
            : base(name, transform, viewPort)
        {
            this.moveKeys = moveKeys;
            this.keyboardManager = (KeyboardManager)IServiceContainer.GetService(typeof(KeyboardManager));
        }


        //any class sub-classing MoveableCamera  should include some (or all) of these methods in its Update() method call
        public virtual void handleInput(GameTime gameTime)
        {

        }
        public virtual void applyCollisionConstraints(GameTime gameTime)
        {

        }
        public virtual void applyAllOtherConstraints(GameTime gameTime)
        {

        }

        public virtual void applyInput(GameTime gameTime)
        {

        }

    }
}
