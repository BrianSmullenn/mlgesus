﻿using Microsoft.Xna.Framework;
using System;

namespace GDLibrary
{
    public class Curve2D
    {
        #region Variables
        private Curve x, y;
        #endregion

        #region Properties
        public Curve X
        {
            get
            {
                return x;
            }
        }
        public Curve Y
        {
            get
            {
                return y;
            }
        }
        #endregion

        public Curve2D(CurveLoopType loopType)
        {
            this.x = new Curve();
            this.x.PreLoop = loopType;
            this.x.PostLoop = loopType;

            this.y = new Curve();
            this.y.PreLoop = loopType;
            this.y.PostLoop = loopType;
        }

        public void Add(Vector2 vector, float time)
        {
            this.x.Keys.Add(new CurveKey(time, vector.X));
            this.y.Keys.Add(new CurveKey(time, vector.Y));
        }

        public void Clear()
        {
            this.x.Keys.Clear(); 
            this.y.Keys.Clear();
        }

        public Vector2 Evaluate(float time, int precision)
        {
            return new Vector2((float)Math.Round(this.x.Evaluate(time), precision),
                         (float)Math.Round(this.y.Evaluate(time), precision));
        }

        public object Clone()
        {
            throw new NotImplementedException();
        }

        public virtual void Dispose()
        {
            throw new NotImplementedException();
        }

    }
}
