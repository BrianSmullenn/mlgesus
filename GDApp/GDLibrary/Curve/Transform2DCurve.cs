﻿using GDApp;
using GDLibrary.Utility;
using Microsoft.Xna.Framework;
using System;

namespace GDLibrary
{
    public class Transform2DCurve
    {
        #region Variables
        public Curve2D positionCurve2D, scaleCurve2D, targetCurve2D;
        public Curve rotationCurve;
        #endregion

        #region Properties
        
        #endregion

        public Transform2DCurve(CurveLoopType loopType)
        {
            this.positionCurve2D = new Curve2D(loopType);

            this.rotationCurve = new Curve();
            this.rotationCurve.PreLoop = loopType;
            this.rotationCurve.PostLoop = loopType;

            this.scaleCurve2D = new Curve2D(loopType);
            this.targetCurve2D = new Curve2D(loopType);
        }

        //add transforms
        public void Add(Vector2 position, Vector2 target, float rotation, Vector2 scale, float time)
        {
            this.positionCurve2D.Add(position, time);
            this.rotationCurve.Keys.Add(new CurveKey(time, rotation));
            this.scaleCurve2D.Add(scale, time);
            this.targetCurve2D.Add(target, time);
        }
        public void AddPosition(Vector2 position, float time)
        {
            this.positionCurve2D.Add(position, time);
        }
        public void AddRotation(float rotation, float time)
        {
            this.rotationCurve.Keys.Add(new CurveKey(time, rotation));
        }
        public void AddScale(Vector2 scale, float time)
        {
            this.scaleCurve2D.Add(scale, time);
        }
        public void AddTarget(Vector2 target, float time)
        {
            this.targetCurve2D.Add(target, time);
        }

        //getting individual transforms (i.e. pos, rot, scale, look) at a time with a precision
        public Vector2 GetPositionAt(float time, int precision)
        {
            return positionCurve2D.Evaluate(time, precision);
        }
        public float GetRotationAt(float time, int precision)
        {
            return (float)Math.Round(rotationCurve.Evaluate(time), precision);
        }
        public Vector2 GetScaleAt(float time, int precision)
        {
            return scaleCurve2D.Evaluate(time, precision);
        }
        public Vector2 GetLookAt(float time, int precision)
        {
            Vector2 look = Vector2.Normalize(positionCurve2D.Evaluate(time, precision) - targetCurve2D.Evaluate(time, precision));
            return MathUtility.Round(look, precision);
        }

        //getting individual transforms (i.e. pos, rot, scale, look) at a time with a DEFAULT precision
        public Vector2 GetPositionAt(float time)
        {
            return GetPositionAt(time, AppData.POSITION_SCALE_ROUND_PRECISION);
        }
        public float GetRotationAt(float time)
        {
            return GetRotationAt(time, AppData.ROTATION_ROUND_PRECISION);
        }
        public Vector2 GetScaleAt(float time)
        {
            return GetScaleAt(time, AppData.POSITION_SCALE_ROUND_PRECISION);
        }
        public Vector2 GetLookAt(float time)
        {
            return GetLookAt(time, AppData.POSITION_SCALE_ROUND_PRECISION);
        }

        public void Clear()
        {
            this.positionCurve2D.Clear(); 
            this.rotationCurve.Keys.Clear();
            this.scaleCurve2D.Clear(); 
        }

        public object Clone()
        {
            throw new NotImplementedException();
        }

        public virtual void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
