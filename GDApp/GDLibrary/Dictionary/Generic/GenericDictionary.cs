﻿using System.Collections.Generic;

namespace GDLibrary.Managers
{
    /*
     * Q. How could we access this dictionary in another class?
     * A. See Main::loadDictionaries()
     */
    public class GenericDictionary<K, V>
    {
        private string name;
        private Dictionary<K, V> dictionary;

        public V this[K key]
        {
            get
            {
                return this.dictionary[key];
            }
        }

        public GenericDictionary(string name)
        {
            this.name = name;
            this.dictionary = new Dictionary<K, V>();
        }

        public void Add(K key, V value)
        {
            if (!this.dictionary.ContainsKey(key))
            {
                this.dictionary.Add(key, value);
            }
        }

        public void Remove(K key)
        {
            if (this.dictionary.ContainsKey(key))
            {
                V value = this.dictionary[key];
                this.dictionary.Remove(key);
                //since null is default for an object we garbage collect the key and value by (normally) setting to null 
                key = default(K); 
                value = default(V); 
            }
        }

        public int Count()
        {
            return this.dictionary.Count;
        }
        public void Clear()
        {
            foreach(K key in this.dictionary.Keys)
            {
                Remove(key);
            }
        }
    }
}
