﻿using GDApp;
using System.Collections.Generic;

namespace GDLibrary.Managers
{
    public class TextureDictionary : IService //renamed from TextureManager since it represents a dictionary in C#
    {
        private Main game;

        private Dictionary<string, TextureData> dictionary;

        public TextureData this[string name]
        {
            get
            {
                name = name.ToLower();
                return this.dictionary[name];
            }
        }

        public TextureDictionary(Main game)
        {
            this.game = game;
            this.dictionary = new Dictionary<string, TextureData>();
        }
        public void Add(TextureData textureData)
        {
            if (!this.dictionary.ContainsKey(textureData.Name))
            {
                this.dictionary.Add(textureData.Name, textureData);
            }
        }

        public void Remove(string name)
        {
            name = name.ToLower();

            if (this.dictionary.ContainsKey(name))
            {
                //once we call remove release texture data
                TextureData tData = this.dictionary[name];
                this.dictionary.Remove(name);
                tData.Dispose();
                tData = null;
            }
        }

        public int Count()
        {
            return this.dictionary.Count;
        }
        public void Dispose()
        {
            List<TextureData> list = new List<TextureData>(dictionary.Values);
            for(int i = 0; i < list.Count; i++)
            {
                list[i].Dispose();
            }

            list.Clear();
            this.dictionary.Clear();
        }
    }
}
