﻿using GDLibrary.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace GDLibrary
{
    public class AnimatedGraphicComponent : GraphicComponent, ICloneable
    {
        public int startFrame, endFrame, frameRateInMS, currentFrame;
        private bool bRepeatAnimation, bPlaying;
        private int elapsedTimeInMs;

        #region Properties
        public int StartFrame
        {
            protected set
            {
                startFrame = value;
            }
            get
            {
                return startFrame;
            }
        }
        public int EndFrame
        {
            protected set
            {
                endFrame = value;
            }
            get
            {
                return endFrame;
            }
        }
        public int FrameRateInMs
        {
            protected set
            {
                frameRateInMS = value;
            }
            get
            {
                return frameRateInMS;
            }
        }

        public new AnimatedTextureData TextureData
        {
            get
            {
                return (AnimatedTextureData)base.TextureData;
            }
            set
            {
                base.TextureData = value;
            }
        }
        #endregion


        public override Color[,] GetColorData()
        {

        //    System.Diagnostics.Debug.WriteLine("Frame:" + currentFrame);

            AnimatedTextureData tData = (AnimatedTextureData)TextureData;

            if (currentFrame < tData.ColorDataList.Count)
                return tData.ColorDataList[currentFrame];
            else //quick fix but possible bug later
                return tData.ColorDataList[0];
        }

        //used when we want to draw the complete image - see Rectangle() dimensions below
        public AnimatedGraphicComponent(AnimatedTextureData textureData, Rectangle sourceRectangle,
            Color color, float depth, SpriteEffects spriteEffects, GraphicEffect graphicEffect)
            : base(textureData, sourceRectangle, color, depth, spriteEffects, graphicEffect)
        {

        }

        public AnimatedGraphicComponent(AnimatedTextureData textureData, Rectangle sourceRectangle, Color color, float depth, SpriteEffects spriteEffects)
            : this(textureData, sourceRectangle, color, depth, spriteEffects, null)
        {

        }


        //used when we want to draw the complete image - see Rectangle() dimensions below
        public AnimatedGraphicComponent(AnimatedTextureData textureData, Color color, float depth, SpriteEffects spriteEffects)
            : this(textureData, new Rectangle(0, 0, textureData.Width, textureData.Height), color, depth, spriteEffects, null)
        {

        }

        public AnimatedGraphicComponent(AnimatedTextureData textureData, Color color, float depth, SpriteEffects spriteEffects, GraphicEffect graphicEffect)
            : this(textureData, new Rectangle(0, 0, textureData.Width, textureData.Height), color, depth, spriteEffects, graphicEffect)
        {

        }

        /// <summary>
        /// PLays an animation sequence within a 1xM frame texture
        /// </summary>
        /// <param name="startFrame">First frame to be seen</param>
        /// <param name="endFrame">One past the last frame to be seen</param>
        /// <param name="frameRateInMS">Speed at which each frame is played</param>
        /// <param name="bRepeatAnimation">Repeat</param>
        public void Play(int startFrame, int endFrame, int frameRateInMS, bool bRepeatAnimation)
        {
            this.startFrame = startFrame;
            this.currentFrame = startFrame;
            this.endFrame = endFrame;
            this.frameRateInMS = frameRateInMS;
            this.bRepeatAnimation = bRepeatAnimation;

            this.bPlaying = true;

            Set(currentFrame);
        }

        public void Pause(bool bPause)
        {
            bPlaying = bPause;
        }
        public void Pause()
        {
            bPlaying = !bPlaying;
        }
        /// <summary>
        /// Sets to a specified frame e.g. the idle frame
        /// </summary>
        /// <param name="frame">A valid frame number</param>
        public void Set(int frame)
        {
            if ((frame >= this.startFrame) && (frame < this.endFrame))
            {
                this.currentFrame = frame;
                this.SourceRectangle = new Rectangle(this.TextureData.Width * this.currentFrame, 0, this.TextureData.Width, this.TextureData.Height);
            }
        }
        public override void Update(GameTime gameTime, DrawableObject drawableObject)
        {


            if(bPlaying)
            {
                elapsedTimeInMs += gameTime.ElapsedGameTime.Milliseconds;

                //within start -> end range
                if (this.currentFrame < this.endFrame) 
                {
                    if(elapsedTimeInMs > frameRateInMS) //time to show next frame?
                    {
                        this.currentFrame++; 
                        this.elapsedTimeInMs = 0; //reset time until next frame update
                        Set(currentFrame); 
                    }
                }
                else //finished
                {
                    if (bRepeatAnimation == true)
                    {
                        Set(this.startFrame); //restart
                    }
                    else
                    {
                        //maybe remove the component?
                        this.ObjectManager.Remove(drawableObject);
                        this.Dispose();
                    }
                }
            }
            base.Update(gameTime, drawableObject);
        }

        public override void Dispose()
        {
            base.Dispose();
        }

        public override void Draw(GameTime gameTime, DrawableObject drawableObject)
        {
            base.Draw(gameTime, drawableObject);
        }

        public new object Clone()
        {
            if(this.GraphicEffect != null)
                return new AnimatedGraphicComponent(this.TextureData, this.SourceRectangle, this.Color, this.Depth, this.SpriteEffects, this.GraphicEffect.DeepCopy());
            else 
                return new AnimatedGraphicComponent(this.TextureData, this.SourceRectangle, this.Color, this.Depth, this.SpriteEffects, null);
        }

    }
}
