﻿using GDLibrary.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace GDLibrary
{
    public class GraphicComponent : ICloneable
    {
        private TextureData textureData;
        private Color color;
        private float depth;
        private Rectangle sourceRectangle;
        private SpriteEffects spriteEffects;

        private GraphicEffect graphicEffect;
        private ObjectManager objectManager;

        #region Properties
        public ObjectManager ObjectManager
        {
            get
            {
                return objectManager;
            }
        }
        public GraphicEffect GraphicEffect
        {
            get
            {
                return graphicEffect;
            }
            set
            {
                graphicEffect = value;
            }
        }
        public Texture2D Texture
        {
            get
            {
                return textureData.Texture;
            }
            set
            {
                textureData.Texture = value;
            }
        }
        public TextureData TextureData
        {
            get
            {
                return textureData;
            }
            set
            {
                textureData = value;
            }
        }
        public Color Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
            }
        }
        public Rectangle SourceRectangle
        {
            get
            {
                return sourceRectangle;
            }
            set
            {
                sourceRectangle = value;
            }
        }
        public float Depth
        {
            get
            {
                return depth;
            }
            set
            {
                depth = value;
            }
        }
        public SpriteEffects SpriteEffects
        {
            get
            {
                return spriteEffects;
            }
            set
            {
                spriteEffects = value;
            }
        }
        #endregion



        public virtual Color[,] GetColorData()
        {
            return textureData.ColorData;
        }

        //used when we want to draw the complete image - see Rectangle() dimensions below
        public GraphicComponent(TextureData textureData, Rectangle sourceRectangle,
            Color color, float depth, SpriteEffects spriteEffects, GraphicEffect graphicEffect)
        {
            //used by Draw()
            this.objectManager = (ObjectManager)IServiceContainer.GetService(typeof(ObjectManager));

            this.textureData = textureData;
            this.color = color;
            this.depth = depth;
            this.sourceRectangle = sourceRectangle;
            this.spriteEffects = spriteEffects;
            this.graphicEffect = graphicEffect;
        }

        public GraphicComponent(TextureData textureData, Rectangle sourceRectangle, Color color, float depth, SpriteEffects spriteEffects)
            : this(textureData, sourceRectangle, color, depth, spriteEffects, null)
        {

        }


        //used when we want to draw the complete image - see Rectangle() dimensions below
        public GraphicComponent(TextureData textureData, Color color, float depth, SpriteEffects spriteEffects)
            : this(textureData, new Rectangle(0, 0, textureData.Width, textureData.Height), color, depth, spriteEffects, null)
        {

        }

        public GraphicComponent(TextureData textureData, Color color, float depth, SpriteEffects spriteEffects, GraphicEffect graphicEffect)
            : this(textureData, new Rectangle(0, 0, textureData.Width, textureData.Height), color, depth, spriteEffects, graphicEffect)
        {

        }

        public virtual void Update(GameTime gameTime, DrawableObject drawableObject)
        {

        }

        public virtual void Draw(GameTime gameTime, DrawableObject drawableObject)
        {
            //does the object have an effect and is it active?
            if ((this.graphicEffect != null) && (this.graphicEffect.IsActive))
            {
                this.graphicEffect.Apply(gameTime, this, drawableObject);
            }

            objectManager.SpriteBatch.Draw(this.textureData.Texture, drawableObject.Transform.Position,
                this.sourceRectangle, this.color, drawableObject.Transform.Rotation, drawableObject.Transform.Origin,
                   drawableObject.Transform.Scale, this.spriteEffects, this.depth);
        }

        public object Clone()
        {
            if(this.graphicEffect != null)
                return new GraphicComponent(this.textureData, this.sourceRectangle, this.color, this.depth, this.spriteEffects, this.graphicEffect.DeepCopy());
            else 
                return new GraphicComponent(this.textureData, this.sourceRectangle, this.color, this.depth, this.spriteEffects, null);
        }

        public virtual void Dispose()
        {
            //throw new NotImplementedException();
        }

    }
}
