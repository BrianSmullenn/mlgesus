﻿using GDLibrary.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace GDLibrary
{
    public class GraphicEffect
    {
        #region Variables
        private bool isActive;
        #endregion

        #region Properties
        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }
        #endregion

        public GraphicEffect(bool isActive)
        {
            //should the effect be applied immediately
            this.isActive = isActive;
        }

        //override this method in any subclass to apply time varying effects to the sprite being drawn
        public virtual void Apply(GameTime gameTime, GraphicComponent graphicsComponent, DrawableObject drawableObject)
        {

        }

        public virtual GraphicEffect DeepCopy()
        {
            return new GraphicEffect(isActive);
        }
    }
}
