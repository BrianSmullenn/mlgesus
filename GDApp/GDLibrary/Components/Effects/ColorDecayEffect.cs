﻿using GDApp;
using Microsoft.Xna.Framework;
using System;

namespace GDLibrary
{
    public class ColorDecayEffect : GraphicEffect
    {
        #region Variables
        private Color startColor, endColor;
        private int lifeSpanInMs;
        private float elapsedTimeInMs;
        #endregion

        #region Properties
        //add properties...
        #endregion

        public ColorDecayEffect(bool isActive, Color startColor, Color endColor, int lifeSpanInMs)
            : base(isActive)
        {
            this.startColor = startColor;
            this.endColor = endColor;
            this.lifeSpanInMs = lifeSpanInMs;
        }

        //override this method in any subclass to apply time varying effects to the sprite being drawn
        public override void Apply(GameTime gameTime, GraphicComponent graphicsComponent, DrawableObject drawableObject)
        {
            //if (drawableObject.Health < 75)
            //{
            //    graphicsComponent.SourceRectangle.X += 
            //}





            applyDecay(gameTime, graphicsComponent);

            //uncomment to see the effect on the draw object
            //applyRotation(drawableObject);
        }

        private void applyRotation(DrawableObject drawableObject)
        {
            drawableObject.Transform.Rotation += AppData.TWENTY_DEGREE_ROTATION_IN_RADIANS;
        }

        private void applyDecay(GameTime gameTime, GraphicComponent graphicsComponent)
        {
            elapsedTimeInMs += gameTime.ElapsedGameTime.Milliseconds;

            if (elapsedTimeInMs <= lifeSpanInMs)
            {
                graphicsComponent.Color = Color.Lerp(this.startColor, this.endColor, (float)Math.Round(elapsedTimeInMs / lifeSpanInMs, 2));
            }
        }

        public override GraphicEffect DeepCopy() //replaces Clone()
        {
            //note - because the parameters are either value types or c# reference types then we dont need to call Clone() on each paramater as in something like DrawableGameObject
            return new ColorDecayEffect(this.IsActive, startColor, endColor, lifeSpanInMs);
        }
    }
}
