﻿using GDLibrary.Utility;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public class CameraTransform2D : Transform2D
    {
        #region Properties
        public new Matrix World
        {
            get 
            {
                Matrix cameraWorld = GeometryUtility.Clone(base.World);
                cameraWorld.M41 *= -1;
                cameraWorld.M42 *= -1;
                return cameraWorld;
            }
        }
        #endregion
        public CameraTransform2D(Vector2 position, float rotation, Vector2 scale, Viewport viewPort)
            : base(Vector2.Zero, position, rotation, scale, new Vector2(viewPort.Width/2, viewPort.Height/2), 
            new Integer2(viewPort.Width, viewPort.Height))
        {
            this.IsCollidable = false;
        }

    }           
}
