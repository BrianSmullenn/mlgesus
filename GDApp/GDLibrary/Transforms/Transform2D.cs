﻿using GDLibrary.Collision;
using GDLibrary.Utility;
using Microsoft.Xna.Framework;
using System;

namespace GDLibrary
{
    public class Transform2D : ICloneable
    {

        public static BSPManager bspManager;

        #region Events
        public delegate void PositionChangedEventHandler(object sender);
        public event PositionChangedEventHandler PositionChanged;
        public virtual void OnPositionChanged()
        {
            if (PositionChanged != null)
            {
                PositionChanged(this);
            }
        }
        #endregion

        #region Variables
        private Vector2 look, originalLook;

        //render related i.e. position, rotatation etc.
        private Vector2 position, origin, scale;
        private Vector2 originalPosition, originalOrigin, originalScale;
        private float rotation, originalRotation;
        
        private Matrix positionMatrix, originMatrix, rotationMatrix, scaleMatrix, world;

        //used when applying input from keyboard, mouse, or controller
        private Vector2 inputMove;
        private float inputRotate, inputScale;

        //bounds related
        public Integer2 originalDimension;
        private Rectangle originalBounds, bounds;

        //draw and collision related
        private bool isCollidable = true, isVisible = true;

        //clean-dirty flags
        private bool isUpdated;
        private int bspSector;
        #endregion

        #region Properties
        public int BSPSector
        {
            get
            {
                return bspSector;
            }
            set
            {
                bspSector = value;
            }
        }
        public Vector2 OriginalPosition
        {
            get
            {
                return originalPosition;
            }
        }
        public Vector2 OriginalScale
        {
            get
            {
                return originalScale;
            }
        }
        public float OriginalRotation
        {
            get
            {
                return originalRotation;
            }
        }
        public Vector2 Look
        {
            get
            {
                return look;
            }
        }
        public bool IsUpdated
        {
            get
            {
                return isUpdated;
            }
        }
        public bool IsVisible
        {
            get
            {
                return isVisible;
            }
            set
            {
                isVisible = value;
            }
        }
        public bool IsCollidable
        {
            get
            {
                return isCollidable;
            }
            set
            {
                isCollidable = value;
            }
        }
        
        public Vector2 InputMove
        {
            get
            {
                return inputMove;
            }
            set
            {
                inputMove = value;
            }

        }
        public float InputRotation
        {
            set
            {
                inputRotate = value;
            }
        }
        public float InputScale
        {
            set
            {
                inputScale = value;
            }
        }
        public Vector2 Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
                positionMatrix = Matrix.CreateTranslation(new Vector3(position, 0));
                isUpdated = true;

                OnPositionChanged(); //notifies all subscribers that transform has changed position
            }
        }
        public Vector2 DeltaPosition
        {
            get
            {
                return position - originalPosition;
            }
        }
   
        public float Rotation
        {
            get
            {
                return rotation;
            }
            set
            {
                rotation = value;
                rotationMatrix = Matrix.CreateRotationZ(rotation); //bug fix - rotation was causing BB not to fit correctly
                isUpdated = true;

                //immediately normalize the vector
                look = Vector2.Normalize(Vector2.Transform(originalLook, rotationMatrix));
            }
        }
        public Vector2 Scale
        {
            get
            {
                return scale;
            }
            set
            {
                scale = value;
                scaleMatrix = Matrix.CreateScale(scale.X, scale.Y, 1);
                isUpdated = true;
            }
        }      
        public Vector2 Origin
        {
            get
            {
                return origin;
            }
            set
            {
                origin = value;
                originMatrix = Matrix.CreateTranslation(new Vector3(-origin, 0));
                isUpdated = true;
            }
        }
        public Rectangle Bounds
        {
            get
            {
                return bounds;
            }
        }
        public Matrix World
        {
            get
            {
                return world;
            }
            protected set
            {
                world = value;
            }
        }
        protected Matrix OriginMatrix
        {
            get
            {
                return originMatrix;
            }
        }
        protected Matrix RotationMatrix
        {
            get
            {
                return rotationMatrix;
            }
        }
        protected Matrix ScaleMatrix
        {
            get
            {
                return scaleMatrix;
            }
        }
        protected Matrix PositionMatrix
        {
            get
            {
                return positionMatrix;
            }
        }
        #endregion

        //used for sprites that need full inital control on rotation, scale etc.
        public Transform2D(Vector2 look, Vector2 position, float rotation, 
            Vector2 scale, Vector2 origin, Integer2 originalDimension)
        {
            this.look = look;

            this.originalLook = look;
            this.originalDimension = originalDimension;
            //note all world transformations are applied to the original bounds, its not an aggregated process
            this.originalBounds = new Rectangle(0, 0, originalDimension.X, originalDimension.Y);
            //useful to store original values in case we ever want to reset the game object or camera
            this.originalPosition = position;
            this.originalRotation = rotation;
            this.originalScale = scale;
            this.originalOrigin = origin;

            Position = position; //moved down - see deltaPosition calculation in Position setter() above
            Origin = origin;
            Scale = scale;
            Rotation = rotation;

            //setup first time around
            updateWorld();
            updateBounds();
        }

        //used by camera
        public Transform2D(Vector2 look, Vector2 position, float rotation, Vector2 scale, Integer2 originalDimension)
            : this(look, position, rotation, scale, Vector2.Zero, originalDimension)
        {
          
        }

        //used for sprites with no initial rotation or scale
        public Transform2D(Vector2 look, Vector2 position, Integer2 originalDimension)
            : this(look, position, 0, Vector2.One, Vector2.Zero, originalDimension)
        {

        }

        //used for full screen background sprites i.e. top left, no rotation, no scale
        public Transform2D(Integer2 originalDimension)
            : this(Vector2.UnitX, Vector2.Zero, 0, Vector2.One, Vector2.Zero, originalDimension)
        {

        }
        public Transform2D(Vector2 origin, Integer2 originalDimension)
            : this(Vector2.UnitX, Vector2.Zero, 0, Vector2.One, origin, originalDimension)
        {

        }


        public Transform2D(float rotation, Vector2 scale, Integer2 originalDimension)
            : this(Vector2.UnitX, Vector2.Zero, rotation, scale, Vector2.Zero, originalDimension)
        {

        }

        public Transform2D(Vector2 look, float rotation, Vector2 scale, Vector2 origin, Integer2 originalDimension)
            : this(look, Vector2.Zero, rotation, scale, origin, originalDimension)
        {

        }

        public virtual void Update()
        {
            if(this.isUpdated) //&& (this.IsImmovable))
            {
                this.isUpdated = false;

                updateWorld();

                updateBounds();

                updateBSP();
            }
        }

        private void updateBSP()
        {
            bspManager.SetSector(this);
        }

        public void reset()
        {
            this.Position = originalPosition;
            this.Rotation = originalRotation;
            this.Scale = originalScale;
            this.Origin = originalOrigin;
        }
   
        //additive changes - used in PlayerObject::applyInput() and MoveableCamera::applyInput()
        public void applyInput(GameTime gameTime)
        {
            if (inputMove != Vector2.Zero)
            {
                this.Position += inputMove;
                //comment the line below out and move the playerobject to see what happens
                inputMove = Vector2.Zero; //reset so value doesnt accumulate across updates()
            }
      
            if (inputRotate != 0)
            {
                this.Rotation += inputRotate;
                inputRotate = 0; //reset so value doesnt accumulate across updates()
            }
       
            if (inputScale != 0)
            {
                this.Scale *= inputScale;
                inputScale = 0; //reset so value doesnt accumulate across updates()
            }
        }

        protected virtual void updateBounds()
        {
            this.bounds
                = CollisionUtility.CalculateTransformedBoundingRectangle(originalBounds, world);
        }
        protected virtual void updateWorld()
        {
            this.world = originMatrix * scaleMatrix * rotationMatrix * positionMatrix;
        }

        public object Clone()
        {
            Transform2D cloneTransform2D = (Transform2D)this.MemberwiseClone();
            return cloneTransform2D;
        }

        public override string ToString()
        {
            return "Position: " + this.position
                    + ", Rotation: " + this.rotation
                         + ", Scale: " + this.scale
                            + "Origin: " + this.origin
                               + ", Bounds: " + this.bounds.ToString()
                                    + ", World: " + this.World.ToString();
        }

        public virtual void Dispose()
        {
            //no user-defined reference types here so nothing to do
        }
    }
}
