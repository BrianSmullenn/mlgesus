﻿using Microsoft.Xna.Framework;
using System;
namespace GDLibrary
{
    public class GameObject : ICloneable
    {
        #region Variables
        private Transform2D transform;
        #endregion

        #region Properties
        public Transform2D Transform
        {
            get
            {
                return transform;
            }
            set
            {
                transform = value;
            }
        }
        #endregion

        public GameObject()
            : this(null)
        {

        }
        
        public GameObject(Transform2D transform)
        {
            this.transform = transform;
        }

        public virtual void Update(GameTime gameTime)
        {
            this.transform.Update();
        }

        public virtual void Draw(GameTime gameTime)
        {

        }

        public object Clone()
        {
            /*
             * We need to call an EXPLICIT Clone() on any user defined reference types e.g. Transform
             * Otherwise when we clone this object the cloned object will point to the same transform
             * and changing any transform properties in an object will change for all clones
             * e.g. imagine all cloned enemies all standing in the same position despite the fact
             * that we explicit set the position of the clone to be somewhere unique.
             */
            Transform2D clonedTransform = (Transform2D)this.transform.Clone();
            return new GameObject(clonedTransform);
        }

        public virtual void Dispose()
        {
            this.transform = null; //See Transform2D::Dispose() for comment on why I nullify this instead of calling Dispose()
        }
    }
}
