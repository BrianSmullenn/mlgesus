﻿using GDLibrary.Managers;
using Microsoft.Xna.Framework;
namespace GDLibrary
{
    public class DrawableObject : GameObject
    {       
        #region Variables
        private GraphicComponent graphicsComponent;

        private ObjectManager objectManager;
        private ScreenManager screenManager;
        private KeyboardManager keyboardManager;
        private SoundManager soundManager;
        private MouseManager mouseManager;    
        #endregion

        #region Properties
        protected MouseManager MouseManager
        {
            get
            {
                return mouseManager;
            }
        }
        protected ObjectManager ObjectManager
        {
            get
            {
                return objectManager;
            }
        }
        protected ScreenManager ScreenManager
        {
            get
            {
                return screenManager;
            }
        }
        protected SoundManager SoundManager
        {
            get
            {
                return soundManager;
            }
            set
            {
                soundManager = value;
            }
        }
        protected KeyboardManager KeyboardManager
        {
            get
            {
                return keyboardManager;
            }
            set
            {
                keyboardManager = value;
            }
        }
        public GraphicComponent GraphicsComponent
        {
            get
            {
                return graphicsComponent;
            }
            set
            {
                graphicsComponent = value;
            }
        }
        #endregion

        public DrawableObject(Transform2D transform, GraphicComponent graphicsComponent)
            : base(transform)
        {
            //contains Draw() saying how to draw the object
            this.graphicsComponent = graphicsComponent;

            //most drawable things need access to the screen i.e. the screen bounds
            this.screenManager = (ScreenManager)IServiceContainer.GetService(typeof(ScreenManager));
            this.keyboardManager = (KeyboardManager)IServiceContainer.GetService(typeof(KeyboardManager));
            this.mouseManager = (MouseManager)IServiceContainer.GetService(typeof(MouseManager));

            this.soundManager = (SoundManager)IServiceContainer.GetService(typeof(SoundManager));
            this.objectManager = (ObjectManager)IServiceContainer.GetService(typeof(ObjectManager));
        }

        public override void Update(GameTime gameTime)
        {
            this.graphicsComponent.Update(gameTime, this);
            base.Update(gameTime);
        }
        public override void Draw(GameTime gameTime)
        {
            this.graphicsComponent.Draw(gameTime, this);
        }
    
        public new DrawableObject Clone()
        {
            Transform2D clonedTransform = (Transform2D)this.Transform.Clone();
            GraphicComponent clonedGraphicsComponent = (GraphicComponent)this.GraphicsComponent.Clone();
            return new DrawableObject(clonedTransform, clonedGraphicsComponent);
        }


        public virtual void handleInput(GameTime gameTime)
        {

        }

        public virtual void applyOtherCollisionConstraints(GameTime gameTime) //e.g. screen
        {

        }
        public virtual void applyCollisionConstraints(GameTime gameTime)  //e.g. bullet against ship
        {

        }

        public virtual void applyInput(GameTime gameTime)
        {

        }

        public override void Dispose()
        {
            this.graphicsComponent.Dispose();
            this.soundManager = null;
            this.mouseManager = null;
            this.keyboardManager = null;
            this.objectManager = null;

            base.Dispose();
                
        }

    }
}
