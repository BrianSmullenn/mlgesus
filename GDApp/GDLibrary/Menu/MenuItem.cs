﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using GDLibrary;


namespace GDLibrary
{
    public class MenuItem
    {
        private static MouseManager mouseManager;

        public String name, text;
        protected Color inactiveColor, activeColor;
        public Rectangle bounds;
        private Vector2 position;
        private Color drawColor;
        public bool bIsMouseOverItem = false;

        public MenuItem(String name, String text, Rectangle bounds, Color inactiveColor, Color activeColor)
        {
            if(mouseManager == null)
                mouseManager = (MouseManager)IServiceContainer.GetService(typeof(MouseManager));


            this.name = name;
            this.text = text;
            this.inactiveColor = inactiveColor;
            this.activeColor = activeColor;
            this.drawColor = inactiveColor;
            this.bounds = bounds;
            this.position = new Vector2(bounds.X, bounds.Y);
        }

        public bool isItemClicked()
        {
            //if the mouse is within the bounds rectangle and the left mouse button is pressed then return true
            return (bIsMouseOverItem && mouseManager.IsFirstLeftButtonPress());
        }


        public void checkMouseOver()
        {
            if (this.bounds.Contains(mouseManager.Bounds))
            {
                bIsMouseOverItem = true;
                drawColor = activeColor;
            }
            else
            {
                bIsMouseOverItem = false;
                drawColor = inactiveColor;
            }
        }

        public void Update(GameTime gameTime) 
        {
            // test for collisions with the mouse pointer
            checkMouseOver();
        }

        public void Draw(SpriteBatch spriteBatch, SpriteFont menuFont)
        {
            spriteBatch.DrawString(menuFont, text, position, drawColor);
        }
    }
}
