﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GDApp;
using GDLibrary.Debug;

namespace GDLibrary.Managers
{
    public class SceneManager : DrawableGameComponent, IService
    {
        private Main game;
        private ObjectManager objectManager;
        private MenuManager menuManager;
        private CameraManager cameraManager;
        private Viewport fullViewport;
        private DebugDrawer debugDrawer;
        private PerformanceDebug debugPerf;

        //temp and debug vars
        private int drawCount;
        private Camera2D camera;


        public SceneManager(Main game)
            : base(game)
        {
            this.game = game;

            this.cameraManager = (CameraManager)IServiceContainer.GetService(typeof(CameraManager));
            this.objectManager = (ObjectManager)IServiceContainer.GetService(typeof(ObjectManager));
            this.menuManager = (MenuManager)IServiceContainer.GetService(typeof(MenuManager));
            this.debugDrawer = (DebugDrawer)IServiceContainer.GetService(typeof(DebugDrawer));
            this.debugPerf = (PerformanceDebug)IServiceContainer.GetService(typeof(PerformanceDebug));

            //record full screen viewport size
            this.fullViewport = game.GraphicsDevice.Viewport;

        }

        public override void Update(GameTime gameTime)
        {
            this.menuManager.Update(gameTime);

            this.objectManager.Update(gameTime);

            if(this.debugPerf != null)
                this.debugPerf.Update(gameTime);

            base.Update(gameTime);
        }
        public override void Draw(GameTime gameTime)
        {
            //reset to full screen viewport for menu etc
            game.GraphicsDevice.Viewport = this.fullViewport;

            //draw the menu
            RenderMenu(gameTime);

            //fps counter
            RenderFPS(gameTime);

            //draw the game screen(s)
            RenderScene(gameTime);

            base.Draw(gameTime);
        }

        private void RenderFPS(GameTime gameTime)
        {
            if (this.debugPerf != null)
            {
                game.SpriteBatch.Begin();
                //since we also add debug bounds rectangles for each drawn object then we divide by 2 to see how many non-debug game objects are drawn
                if ((this.debugDrawer != null) || (this.cameraManager.Size() != 1))
                    drawCount /= 2;

                this.debugPerf.Draw(gameTime, drawCount);
                game.SpriteBatch.End();
            }
        }
        private void RenderMenu(GameTime gameTime)
        {
            game.SpriteBatch.Begin();
            this.menuManager.Draw(gameTime);
            game.SpriteBatch.End();
        }

        private void RenderScene(GameTime gameTime)
        {
            drawCount = 0;

            //repeat the draw for every split camera
            for (int i = 0; i < this.cameraManager.Size(); i++)
            {
                this.camera = this.cameraManager[i];

                //we MUST set the gfx device to use the camera viewport 
                game.GraphicsDevice.Viewport = camera.Viewport;

                game.SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.LinearClamp, DepthStencilState.Default, RasterizerState.CullNone, null, camera.Transform.World);
                drawCount += this.objectManager.Draw(gameTime, camera);

                //draw bounding boxes - remove for release
                if (debugDrawer != null)
                    debugDrawer.Draw(gameTime);

                game.SpriteBatch.End();

            }
        }
    }
}
