﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using GDApp;

namespace GDLibrary.Managers
{
    public class ObjectManager : IService
    {
        #region Variables
        private Main game;
        private List<GameObject> updateList;        //what to update
        private List<GameObject> drawList;           //what to draw
        private HashSet<GameObject> drawSet;
        private List<GameObject> disposeList;       //what has been removed and needs a Dispose() call
        private CameraManager cameraManager;
        private bool bPause;

        //temp vars
        private GameObject tempObject;
        #endregion

        #region Properties
        public bool Pause
        {
            get
            {
                return bPause;
            }
            set
            {
                bPause = value;
            }
        }
        public List<GameObject> DrawList
        {
            get
            {
                return drawList;
            }
        }
        public List<GameObject> UpdateList
        {
            get
            {
                return updateList;
            }
        }          
        public SpriteBatch SpriteBatch
        {
            get
            {
                return game.SpriteBatch;
            }
        }
        #endregion
        
        public ObjectManager(Main game)
        {
            this.game = game;

            //what to update
            this.updateList = new List<GameObject>();
            
            //what to draw
            this.drawList = new List<GameObject>();

            //used by setDrawList() to protect Add() access to drawList
            this.drawSet = new HashSet<GameObject>();

            //what has been removed and needs a Dispose() call
            this.disposeList = new List<GameObject>();
            
            this.cameraManager = (CameraManager)IServiceContainer.GetService(typeof(CameraManager));

        }

        public void Add(GameObject gameObject)
        {
            this.updateList.Add(gameObject);
        }
        public void Add(List<GameObject> list)
        {
            foreach(GameObject o in list)
            {
                this.updateList.Add(o);
            }
            
        }
        public void Remove(GameObject gameObject)
        {
            if (gameObject != null)
            {
                //dont update
                this.updateList.Remove(gameObject);

                //this object will have its Dispose() method called in the next update
                this.disposeList.Add(gameObject);
            }
        }
        public void Dispose()
        {
            for(int i = 0; i < this.updateList.Count; i++)
            {
                GameObject obj = this.updateList[i];
                obj.Dispose();
                this.updateList.RemoveAt(i);
            }

            this.updateList = null;

            this.drawList.Clear();
            this.drawList = null;
        }
        //update everything i.e. even the things off screen
        public void Update(GameTime gameTime)
        {
            if (!bPause)
            {
                disposeUnused();

                for (int i = 0; i < updateList.Count; i++)
                {
                    updateList[i].Update(gameTime);
                }
            }
        }
        public int Draw(GameTime gameTime, Camera2D camera)
        {
            if (!bPause)
            {
                //cull all objects not visible to specified camera
                setDrawList(camera);

                //draw objects
                for (int i = 0; i < drawList.Count; i++)
                {
                    drawList[i].Draw(gameTime);
                }

                return this.drawList.Count;
            }

            return 0;
        }

        //any time we remove something from the screen we add it to this list and the Dispose() method for the object will release all resources used by the object
        private void disposeUnused()
        {
            if (this.disposeList.Count > 0)
            {
                for (int i = 0; i < this.disposeList.Count; i++)
                {
                    this.disposeList[i].Dispose();
                }
                this.disposeList.Clear(); //clear for next update cycle
            }
        }
     
        //only draw what the camera(s) can see
        private void setDrawList(Camera2D camera)
        {
            this.drawList.Clear(); //set of all drawable objects
            this.drawSet.Clear(); //set to optimise the contains() lookup

            //can the camera see the object?
            for (int i = 0; i < updateList.Count; i++)
            {
                tempObject = updateList[i];
                if(tempObject is DrawableObject && tempObject.Transform.IsVisible) //we can turn objects on/off using this
                {
                    if (tempObject.Transform.Bounds.Intersects(camera.Transform.Bounds) && !this.drawSet.Contains(tempObject))
                    {
                        this.drawList.Add(tempObject);
                        this.drawSet.Add(tempObject);
                    }
                } //end if
            }//end for
        }
    

    
    
    }
}
