﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GDLibrary.Utility;
using GDLibrary.Managers;
using GDApp;


namespace GDLibrary
{
    public class MenuManager: IService
    {
        #region Variables
        protected List<MenuItem> menuItemList;

        public Main game;
        public SpriteFont menuFont;

        private Texture2D[] menuTextures;
        private Rectangle textureRectangle;

        private MenuItem menuResume, menuExit, menuAudio;
        private MenuItem menuVolumeUp, menuVolumeDown, menuBack;

        protected int currentMenuTextureIndex = 0; //0 = main, 1 = volume
        private bool bPause;
        private ObjectManager objectManager;
        private KeyboardManager keyboardManager;
        private MouseManager mouseManager;
        private ScreenManager screenManager;
        #endregion

        #region PROPERTIES
        public bool Pause
        {
            get
            {
                return bPause;
            }
            set
            {
                bPause = value;
            }
        }
        #endregion



        public MenuManager(Main game, String[] strMenuTextures, SpriteFont menuFont, Integer2 textureBorderPadding) 
        {
            this.game = game;
            this.bPause = false; //show menu initially

            //create an array of textures
            this.menuTextures = new Texture2D[strMenuTextures.Length];

            //nmcg - load the textures
            for (int i = 0; i < strMenuTextures.Length; i++)
            {
                this.menuTextures[i] = game.Content.Load<Texture2D>(@"" + strMenuTextures[i]);
            }

            //load menu font
            this.menuFont = menuFont;

            //stores all menu item (e.g. Save, Resume, Exit) objects
            this.menuItemList = new List<MenuItem>();

            this.objectManager = (ObjectManager)IServiceContainer.GetService(typeof(ObjectManager));
            this.keyboardManager = (KeyboardManager)IServiceContainer.GetService(typeof(KeyboardManager));
            this.mouseManager = (MouseManager)IServiceContainer.GetService(typeof(MouseManager));
            this.screenManager = (ScreenManager)IServiceContainer.GetService(typeof(ScreenManager));

            //set the texture background to occupy the entire screen dimension, less any padding
            this.textureRectangle = new Rectangle(textureBorderPadding.X, textureBorderPadding.Y,
                screenManager.Bounds.Width - 2 * textureBorderPadding.X,
                screenManager.Bounds.Height - 2 * textureBorderPadding.Y);

            //pause at the start so the objects dont update based on user-input WHEN the menu is shown
            this.objectManager.Pause = true;

            //add the basic items - "Resume", "Save", "Exit"
           InitialiseMenuOptions();
           ShowMainMenuScreen();
        }

        private void InitialiseMenuOptions()
        {
            //add the menu items to the list
            this.menuResume = new MenuItem(MenuData.MENU_RESUME, MenuData.MENU_RESUME,
                new Rectangle(50, 50, 120, 30), MenuData.MENU_INACTIVE_COLOR, MenuData.MENU_ACTIVE_COLOR);
            this.menuAudio = new MenuItem(MenuData.MENU_AUDIO, MenuData.MENU_AUDIO,
               new Rectangle(50, 100, 65, 30), MenuData.MENU_INACTIVE_COLOR, MenuData.MENU_ACTIVE_COLOR);
            this.menuExit = new MenuItem(MenuData.MENU_EXIT, MenuData.MENU_EXIT,
                new Rectangle(50, 150, 50, 30), MenuData.MENU_INACTIVE_COLOR, MenuData.MENU_ACTIVE_COLOR);

            //second menu - audio settings
            this.menuVolumeUp = new MenuItem(MenuData.MENU_VOLUMEUP, MenuData.MENU_VOLUMEUP,
               new Rectangle(550, 50, 65, 30), MenuData.MENU_INACTIVE_COLOR, MenuData.MENU_ACTIVE_COLOR);
            this.menuVolumeDown = new MenuItem(MenuData.MENU_VOLUMEDOWN, MenuData.MENU_VOLUMEDOWN,
               new Rectangle(550, 100, 65, 30), MenuData.MENU_INACTIVE_COLOR, MenuData.MENU_ACTIVE_COLOR);
            this.menuBack = new MenuItem(MenuData.MENU_BACK, MenuData.MENU_BACK,
                new Rectangle(550, 150, 50, 30), MenuData.MENU_INACTIVE_COLOR, MenuData.MENU_ACTIVE_COLOR);
        }

        private void ShowMainMenuScreen()
        {
            Add(menuResume);
            Add(menuAudio);
            Add(menuExit);
            currentMenuTextureIndex = 0;
        }

        private void ShowVolumeMenuScreen()
        {
            Add(menuVolumeUp);
            Add(menuVolumeDown);
            Add(menuBack);
            currentMenuTextureIndex = 1;
        }
                                                                                                                                          
        public void Add(MenuItem theMenuItem) 
        {
            menuItemList.Add(theMenuItem);
        }

        public void Remove(MenuItem theMenuItem)
        {
            menuItemList.Remove(theMenuItem);
        }

        public void RemoveAll()
        {
            menuItemList.Clear();
        }

        public void Update(GameTime gameTime)
        {
            checkPauseNotPressed();

            if (!bPause)
            {
                for (int i = 0; i < menuItemList.Count; i++)
                {
                    //test for collisions with the mouse
                    menuItemList[i].Update(gameTime);
                    if (menuItemList[i].isItemClicked())
                    {
                        MenuAction(menuItemList[i].name);
                        break; // can only click one item at a time so dont bother continuing
                    }
                }
            }
        }

        private void checkPauseNotPressed()
        {
            if (bPause && this.keyboardManager.IsFirstKeyPress(KeyData.KEY_PAUSE_SHOW_MENU))
            {
                bPause = false;
                this.objectManager.Pause = true;
            }
        }

        public void Draw(GameTime gameTime) 
        {
            if(!bPause)
            { 
                //draw whatever background we expect to see based on what menu or sub-menu we are viewing
                game.SpriteBatch.Draw(menuTextures[currentMenuTextureIndex], textureRectangle, Color.White);

                //draw the text on top of the background
                for (int i = 0; i < menuItemList.Count; i++)
                {
                    menuItemList[i].Draw(game.SpriteBatch, menuFont);
                }
            }
        }

        //perform whatever actions are listed on the menu
        private void MenuAction(String name)
        {
            if (name.Equals(MenuData.MENU_RESUME))
            {
                this.bPause = true;
                this.objectManager.Pause = false;
            }
            else if (name.Equals(MenuData.MENU_AUDIO))
            {
                Remove(menuResume);
                Remove(menuAudio);
                Remove(menuExit);
                ShowVolumeMenuScreen();
            }
            else if (name.Equals(MenuData.MENU_EXIT))
            {
                this.game.Exit();            
            }
            else if (name.Equals(MenuData.MENU_BACK))
            {
                Remove(menuVolumeUp);
                Remove(menuVolumeDown);
                Remove(menuBack);

                ShowMainMenuScreen();
            }
        }
    }
}
