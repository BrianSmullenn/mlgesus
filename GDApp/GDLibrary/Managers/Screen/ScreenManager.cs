﻿using Microsoft.Xna.Framework;
using GDApp;
using System;

namespace GDLibrary.Managers
{
    public class ScreenManager : GameComponent, IService
    {
        private Main game;

        private Rectangle bounds;
        private ScreenResolutionType resolutionType;
        private int framesPerSecond;

        #region Properties
        public ScreenResolutionType ScreenResolutionType
        {
            get
            {
                return resolutionType;
            }
        }
        public Rectangle Bounds  //bug - nmcg
        {
            get
            {
                return bounds;
            }
        }
        public int Width
        {
            get
            {
                return bounds.Width;
            }
        }
        public int Height
        {
            get
            {
                return bounds.Height;
            }
        }
        #endregion

        public ScreenManager(Main game, ScreenResolutionType resolutionType, int framesPerSecond)
            : base(game)
        {
            this.game = game;
            this.resolutionType = resolutionType; //store in case we ever want to print out screen details
            this.framesPerSecond = framesPerSecond;

            SetFrameRate(game, framesPerSecond);
            SetResolution(this.resolutionType);
        }

        public void SetFrameRate(Main game, int framesPerSecond)
        {
            if (framesPerSecond != -1)
            {
                game.IsFixedTimeStep = true; //call update at a fixed rate (indicated below)
                game.TargetElapsedTime = new TimeSpan(0, 0, 0, 0, (int)(1000.0f / framesPerSecond));
            }
            else
            {
                game.IsFixedTimeStep = false; //call update (and by implication draw()) as quickly as the gfx card allows.
                
                /*
                 * In a double buffering scheme the backbuffer is used to assemble the frame contents, while the front buffer displays it.
                 * Vertical retrace is defined as the exact moment when the monitor refreshes and requires a new frame.
                 * When this moment occurs the back and front buffers are swapped and the user sees the assembled frame.
                 * 
                 * If we disable vertical retrace then we no longer wait for this exact refresh moment from the monitor.
                 * Instead we swap the buffers - and output the assembled frame - as quickly as the CPU and GPU will allow.
                 */
                game.Graphics.SynchronizeWithVerticalRetrace = false; 
            }
        }

        public void SetResolution(ScreenResolutionType resolutionType)
        {
            ScreenResolutionData resolutionData = ScreenManagerHelper.getScreenResolution(this.resolutionType);
            game.Graphics.PreferredBackBufferWidth = resolutionData.Width;
            game.Graphics.PreferredBackBufferHeight = resolutionData.Height;
            this.bounds = new Rectangle(0, 0, game.Graphics.PreferredBackBufferWidth, game.Graphics.PreferredBackBufferHeight);
            game.Graphics.ApplyChanges(); //load new changes
        }

        public override void Update(GameTime gameTime)
        {
            System.Diagnostics.Debug.WriteLine("Running slowly: " + gameTime.IsRunningSlowly);
            base.Update(gameTime);
        }
    }
}
