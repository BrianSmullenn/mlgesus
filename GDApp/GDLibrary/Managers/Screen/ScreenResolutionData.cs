﻿using Microsoft.Xna.Framework;

namespace GDLibrary
{
    /// <summary>
    /// An inner class to store name-resolution pairs
    /// </summary>
    public class ScreenResolutionData
    {
        private string name;
        private Vector2 resolution;
        private string aspectRatio;

        #region PROPERTIES
        public int Width
        {
            get
            {
                return (int)resolution.X;
            }
        }
        public int Height
        {
            get
            {
                return (int)resolution.Y;
            }
        }
        public string Name
        {
            get
            {
                return name;
            }

        }
        public string AspectRatio
        {
            get
            {
                return aspectRatio;
            }
        }
        public float AspectRatioValue
        {
            get
            {
                return (float)resolution.X / resolution.Y;
            }
        }
        #endregion

        public ScreenResolutionData(string name, Vector2 resolution, string aspectRatio)
        {
            this.name = name;
            this.resolution = resolution;
            this.aspectRatio = aspectRatio;
        }

        public override string ToString()
        {
            return "Screen Resolution Data: " + this.name
                + ", Resolution: " + this.resolution.ToString()
                    + ", Aspect Ratio: " + this.aspectRatio;
                    
        }
    }
}
