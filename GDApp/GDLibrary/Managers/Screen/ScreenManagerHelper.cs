﻿using Microsoft.Xna.Framework;

namespace GDLibrary
{
    /// <summary>
    /// Stores info on typical screen resolutions
    /// </summary>
    public static class ScreenManagerHelper
    {
        private static ScreenResolutionData[] resolutionArray = 
        {
            new ScreenResolutionData("vga", new Vector2(640, 480), "4:3"),
            new ScreenResolutionData("svga", new Vector2(800, 600), "4:3"),
            new ScreenResolutionData("wvga", new Vector2(800, 480), "16:9"),
            new ScreenResolutionData("xga", new Vector2(1024, 768), "4:3"),
            new ScreenResolutionData("hd720", new Vector2(1280, 720), "4:3"),
            new ScreenResolutionData("wxga", new Vector2(1280, 768), "8:5"),
            new ScreenResolutionData("sxga", new Vector2(1280, 1024), "5:4"),
            new ScreenResolutionData("uxga", new Vector2(1600, 1200), "4:3"),
            new ScreenResolutionData("hd1080", new Vector2(1920, 1080), "16:9"),
            new ScreenResolutionData("qxga", new Vector2(2048, 1536), "4:3")
                  //add any new resolutions here...
        };

        //cycles through resolutions available and returns details of next available resolution
        public static ScreenResolutionData getScreenResolution(ScreenResolutionType resolution)
        {
            return resolutionArray[(int)resolution];
        }
    }
}
