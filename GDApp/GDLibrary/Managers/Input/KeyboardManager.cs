﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using GDApp;

namespace GDLibrary
{
    public class KeyboardManager : GameComponent, IService
    {
        public KeyboardState newState, oldState;

        public KeyboardManager(Main game)
            : base(game)
        {
        }
        public override void Update(GameTime gameTime)
        {
            oldState = newState;
            newState = Keyboard.GetState();
            base.Update(gameTime);
        }
        public bool IsFirstKeyPress(Keys key)
        {
            if (oldState.IsKeyUp(key) && newState.IsKeyDown(key))
                return true;

            return false;
        }

        public bool IsKeyPressed()
        {
            if (newState.GetPressedKeys().Length != 0)
                return true;

            return false;

        }

        public bool IsKeyDown(Keys key)
        {
            return newState.IsKeyDown(key);
        }

        public bool IsKeyUp(Keys key)
        {
            return newState.IsKeyUp(key);
        }
    }
}
