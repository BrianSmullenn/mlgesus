﻿using GDLibrary.Utility;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using GDApp;


namespace GDLibrary
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class MouseManager : Microsoft.Xna.Framework.GameComponent, IService
    {
        private MouseState oldState, newState;
        private Main game;
        private Rectangle bounds;
        private bool cameraPositionChanged;

        #region PROPERTIES
        public Vector2 Position
        {
            get
            {
                return new Vector2(newState.X, newState.Y) + this.game.ActiveCamera.Transform.DeltaPosition;
            }
        }

        public Rectangle Bounds
        {
            get
            {
                bounds = new Rectangle(newState.X, newState.Y, 1, 1);

                if (cameraPositionChanged) 
                {           
                    /*
                    * If we move the camera the bounds must reflect the new screen position and not be confined to
                    * the original dimensions of the screen e.g. (0, 0, Width, Height)
                    * For example, imagine what happens when the camera completely leaves the start screen. 
                    * What value should the bounds rectangle have then?
                    */
   
                    GeometryUtility.Add(ref bounds, this.game.ActiveCamera.Transform.DeltaPosition);
                    this.cameraPositionChanged = false;
                }
                return bounds;
            }
        }

        public MouseState NEWSTATE
        {
            get
            {
                return newState;
            }
        }

        public MouseState OLDSTATE
        {
            get
            {
                return oldState;
            }
        }
        #endregion

        public MouseManager(Main game, bool isMouseVisible)
            : base(game)
        {
            this.game = game;
            this.newState = Mouse.GetState();
            game.IsMouseVisible = isMouseVisible;

            RegisterForEvents();

        }

        #region Event Handling
        private void RegisterForEvents()
        {
            this.game.ActiveCamera.Transform.PositionChanged += Transform_PositionChanged;
        }
        void Transform_PositionChanged(object sender)
        {
            this.cameraPositionChanged = true;
        }
        #endregion

        public override void Initialize()
        {
            // TODO: Add your initialization code here

            base.Initialize();
        }

        #region INPUT
        public Vector2 GetPosition()
        {
            return new Vector2(newState.X, newState.Y);

        }
        public int GetScrollWheelValue()
        {
            return newState.ScrollWheelValue;
        }

        public bool IsLeftButtonPressed()
        {
            return newState.LeftButton.Equals(ButtonState.Pressed);
        }

        public bool IsMiddleButtonPressed()
        {
            return newState.MiddleButton.Equals(ButtonState.Pressed);
        }

        public bool IsRightButtonPressed()
        {
            return newState.RightButton.Equals(ButtonState.Pressed);
        }

        //are the buttons pressed now but released in the previous update
        public bool IsLeftButtonPressedOnce()
        {
            return(newState.LeftButton.Equals(ButtonState.Pressed) && (oldState.LeftButton.Equals(ButtonState.Released)));
        }

        public bool IsMiddleButtonPressedOnce()
        {
            return (newState.MiddleButton.Equals(ButtonState.Pressed) && (oldState.MiddleButton.Equals(ButtonState.Released)));
        }

        public bool IsRightButtonPressedOnce()
        {
            return (newState.RightButton.Equals(ButtonState.Pressed) && (oldState.RightButton.Equals(ButtonState.Released)));
        }

        public bool IsMouseStateChanged()
        {
            return (!newState.Equals(oldState)); //are states different?
        }

        public int GetScrollWheelDelta()
        {
            if (!newState.Equals(oldState))
            {
                return newState.ScrollWheelValue - oldState.ScrollWheelValue;
            }
            return 0;
        }

        #endregion

        public override void Update(GameTime gameTime)
        {
            oldState = newState;

            newState = Mouse.GetState();

            base.Update(gameTime);
        }

        private void UpdateBounds()
        {
            throw new System.NotImplementedException();
        }


        /// <summary>
        /// Centre the mouse on the screen (based on screen resolution)
        /// </summary>
        /// <param name="centre"></param>
        public void SetCentre(Vector2 centre)
        {
            Mouse.SetPosition((int)centre.X, (int)centre.Y);
        }

        /// <summary>
        /// Used to calculate the pitch (deltaY) and yaw (deltaX) for a moveable camera
        /// </summary>
        /// <param name="centre">Centre position of the screen (based on screen resolution)</param>
        /// <returns>Change in X and Y values</returns>
        public Vector2 getDeltaValue(Vector2 centre)
        {
            return new Vector2(newState.X - centre.X, newState.Y - centre.Y);   
        }

        public bool IsStateChanged()
        {
            return !newState.Equals(oldState);
        }

        public bool IsFirstLeftButtonPress()
        {
            return (newState.LeftButton.Equals(ButtonState.Pressed) && oldState.LeftButton.Equals(ButtonState.Released));
        }
        public bool IsFirstRightButtonPress()
        {
            return (newState.RightButton.Equals(ButtonState.Pressed) && oldState.RightButton.Equals(ButtonState.Released));
        }
    }
}