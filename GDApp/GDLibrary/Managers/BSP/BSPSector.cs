﻿using System;
using Microsoft.Xna.Framework;
using System.Xml.Serialization;

namespace GDLibrary
{
    /// <summary>
    /// Stores the rectangle and sector number for each collision sector onscreen.
    /// The greater the number of collision sectors, the less CDCR tests and single sprite must perform.
    /// </summary>
    /// 
    [Serializable]
    public class BSPSector : ICloneable
    {
        private static int currentSectorIndex = 0;     //index of the current sector, increases as we add, maximum is totalSectorNumber
        private Rectangle sectorBounds;    //bounding rectangle for each sector
        private int sectorNumber;             //sector number for each rectangle (must be power of 2)

        private Color color;
        public static Color ACTIVE_COLOR = Color.Green;
        public static Color ERROR_COLOR = Color.Red;
        public static Color PLACED_COLOR = Color.Yellow;

        //only variables with getter/setter properties will be serialized
        public int SectorNumber
        {
            get
            {
                return sectorNumber;
            }
            set
            {
                sectorNumber = value;
            }
        }

        public Rectangle SectorBounds
        {
            get
            {
                return sectorBounds;
            }
            set
            {
                sectorBounds = value;
            }
        }
        [XmlIgnoreAttribute]
        public Color Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
            }
        }

        [XmlIgnoreAttribute]
        public static int CurrentSectorIndex
        {
            get
            {
                return currentSectorIndex;
            }
            set
            {
                currentSectorIndex = value;
            }
        }

        [XmlIgnoreAttribute]
        public int SectorBoundsX
        {
            get
            {
                return sectorBounds.X;
            }
            set
            {
                sectorBounds.X = value;
            }
        }

        [XmlIgnoreAttribute]
        public int SectorBoundsY
        {
            get
            {
                return sectorBounds.Y;
            }
            set
            {
                sectorBounds.Y = value;
            }
        }

        [XmlIgnoreAttribute]
        public int Width
        {
            get
            {
                return sectorBounds.Width;
            }
            set
            {
                sectorBounds.Width = value > 0 ? value : 100;
            }
        }

        [XmlIgnoreAttribute]
        public int Height
        {
            get
            {
                return sectorBounds.Height;
            }
            set
            {
                sectorBounds.Height = value > 0 ? value : 100;
            }
        }

        public BSPSector()
        {
            this.sectorNumber = 0;
            this.sectorBounds = new Rectangle(0, 0, 100, 100);
            this.color = ACTIVE_COLOR;
        }

        public BSPSector(float x, float y, int width, int height)
        {
            //every new sector gets a power of two sector number, e.g. 1, 2, 4, 8 etc
            this.sectorNumber = (int)Math.Pow(2, currentSectorIndex++);
            this.sectorBounds = new Rectangle((int)x, (int)y, width, height);
            this.color = ACTIVE_COLOR;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if (this.GetType() != obj.GetType())
                return false;
            
            BSPSector other = (BSPSector)obj;
            return this.sectorBounds.Equals(other.SectorBounds);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public Object Clone()
        {
            return (BSPSector)this.MemberwiseClone();
        }

        public void setColor(Color color)
        {
            this.color = color;
        }

    }
}
