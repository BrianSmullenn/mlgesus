﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using GDApp;

namespace GDLibrary
{
    public class BSPLayoutManager : DrawableGameComponent, IService
    {
        #region Variables
        private Main game;
        private KeyboardManager keyboardManager;
        private MouseManager mouseManager;
        private BSPManager bspManager;

        private BSPSector newSector;
        private Texture2D texture;
        private bool isActive = true;
        private SpriteFont font;
        private int offset = 15;

        //temp vars
        private int width = 200, height = 200;
        private int coarseMoveIncrement = 5, fineMoveIncrement = 1, coarseSizeIncrement = 5, fineSizeIncrement = 1;
        private bool bPause;

        #endregion

        #region Properties
        public bool Pause
        {
            get
            {
                return bPause;
            }
            set
            {
                bPause = value;
            }
        }
        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }
        #endregion

        public BSPLayoutManager(Main game, Texture2D texture, SpriteFont font,
            BSPManager bspManager, bool isActive)
            : base(game)
        {
            this.game = game;
            this.texture = texture;
            this.font = font;
            this.bspManager = bspManager;
            this.isActive = isActive;

            this.keyboardManager = (KeyboardManager)IServiceContainer.GetService(typeof(KeyboardManager));
            this.mouseManager = (MouseManager)IServiceContainer.GetService(typeof(MouseManager));
        }


        public new void Dispose()
        {
            bspManager.Save();
            keyboardManager = null;
            mouseManager = null;
        }
       

        private void TestEdit()
        {
            BSPSector sector = null;

            if (this.keyboardManager.IsFirstKeyPress(Keys.Home))
            {
                for (int i = 0; i < bspManager.Size(); i++)
                {
                    sector = bspManager[i];
                    if (sector.SectorBounds.Contains(this.mouseManager.Bounds))
                    {
                        bspManager.Remove(sector);
                        newSector = sector;
                        newSector.setColor(BSPSector.ACTIVE_COLOR);
                    }
                }
            }
        }

        private void TestDelete()
        {
            BSPSector sector = null;

            if (this.keyboardManager.IsFirstKeyPress(Keys.Delete))
            {
                for (int i = 0; i < bspManager.Size(); i++)
                {
                    sector = bspManager[i];
                    if (sector.SectorBounds.Contains(this.mouseManager.Bounds))
                        bspManager.Remove(sector);
                }

                if (newSector != null)
                {
                    if (newSector.SectorBounds.Contains(this.mouseManager.Bounds))
                        newSector = null;
                }
            }
        }

        private void TestOverlap()
        {
            BSPSector sector = null;
            if (newSector != null)
            {
                for (int i = 0; i < bspManager.Size(); i++)
                {
                    sector = bspManager[i];
                    if (newSector.SectorBounds.Intersects(sector.SectorBounds))
                        sector.setColor(BSPSector.ERROR_COLOR);
                    else
                        sector.setColor(BSPSector.PLACED_COLOR);
                }
            }
        }

        private void AddNewSector()
        {
            if ((newSector != null) && (!newSector.Color.Equals(BSPSector.ERROR_COLOR)))
            {
                if (this.keyboardManager.IsFirstKeyPress(Keys.End))
                {
                    newSector.setColor(BSPSector.PLACED_COLOR);
                    bspManager.Add((BSPSector)newSector.Clone());                

                    //store so that next sector will have same width and height
                    width = newSector.SectorBounds.Width;
                    height = newSector.SectorBounds.Height;

                    newSector = null;
                }
            }
        }

        private void CreateNewSector()
        {
            if (this.keyboardManager.IsFirstKeyPress(Keys.Insert))
            {
                this.newSector = new BSPSector(this.mouseManager.Position.X - width / 2,
                    this.mouseManager.Position.Y - height / 2, width, height);
            }
        }

        private void UpdateSizePosition()
        {
            if (newSector != null)
            {
                if ((!this.keyboardManager.IsKeyDown(KeyData.KEY_LAYOUT_MANAGER_FINE_REPOSITION))
                   && (!this.keyboardManager.IsKeyDown(KeyData.KEY_LAYOUT_MANAGER_COARSE_RESIZE))
                   && (!this.keyboardManager.IsKeyDown(KeyData.KEY_LAYOUT_MANAGER_FINE_RESIZE)))
                {
                    GetCoarseMovement(coarseMoveIncrement); // U/D/L/R only
                }
                else if ((this.keyboardManager.IsKeyDown(KeyData.KEY_LAYOUT_MANAGER_FINE_REPOSITION))
                    && (!this.keyboardManager.IsKeyDown(KeyData.KEY_LAYOUT_MANAGER_COARSE_RESIZE))
                    && (!this.keyboardManager.IsKeyDown(KeyData.KEY_LAYOUT_MANAGER_FINE_RESIZE)))
                {
                    GetFineMovement(fineMoveIncrement); // F5 + U/D/L/R
                }
                else if ((!this.keyboardManager.IsKeyDown(KeyData.KEY_LAYOUT_MANAGER_FINE_REPOSITION))
                    && (this.keyboardManager.IsKeyDown(KeyData.KEY_LAYOUT_MANAGER_COARSE_RESIZE))
                        && (!this.keyboardManager.IsKeyDown(KeyData.KEY_LAYOUT_MANAGER_FINE_RESIZE)))
                {
                    CoarseSize(coarseSizeIncrement); // F6 + U/D/L/R
                }
                else if ((!this.keyboardManager.IsKeyDown(KeyData.KEY_LAYOUT_MANAGER_FINE_REPOSITION))
                    && (!this.keyboardManager.IsKeyDown(KeyData.KEY_LAYOUT_MANAGER_COARSE_RESIZE))
                        && (this.keyboardManager.IsKeyDown(KeyData.KEY_LAYOUT_MANAGER_FINE_RESIZE)))
                {
                    FineSize(fineSizeIncrement); // F7 + U/D/L/R
                }
             
            }
        }

        private void GetFineMovement(int increment)
        {
            if (this.keyboardManager.IsFirstKeyPress(KeyData.KEY_LAYOUT_MANAGER_UP))
            {
                newSector.SectorBoundsY -= increment;
            }
            else if (this.keyboardManager.IsFirstKeyPress(KeyData.KEY_LAYOUT_MANAGER_DOWN))
            {
                newSector.SectorBoundsY += increment;
            }
            if (this.keyboardManager.IsFirstKeyPress(KeyData.KEY_LAYOUT_MANAGER_LEFT))
            {
                newSector.SectorBoundsX -= increment;
            }
            if (this.keyboardManager.IsFirstKeyPress(KeyData.KEY_LAYOUT_MANAGER_RIGHT))
            {
                newSector.SectorBoundsX += increment;
            }
        }

        private void GetCoarseMovement(int increment)
        {
            if (this.keyboardManager.IsKeyDown(KeyData.KEY_LAYOUT_MANAGER_UP))
            {
                newSector.SectorBoundsY -= increment;
            }
            else if (this.keyboardManager.IsKeyDown(KeyData.KEY_LAYOUT_MANAGER_DOWN))
            {
                newSector.SectorBoundsY += increment;
            }
            if (this.keyboardManager.IsKeyDown(KeyData.KEY_LAYOUT_MANAGER_LEFT))
            {
                newSector.SectorBoundsX -= increment;
            }
            if (this.keyboardManager.IsKeyDown(KeyData.KEY_LAYOUT_MANAGER_RIGHT))
            {
                newSector.SectorBoundsX += increment;
            }
        }

        private void CoarseSize(int increment)
        {
            if (this.keyboardManager.IsKeyDown(KeyData.KEY_LAYOUT_MANAGER_UP))
            {
                newSector.Height -= increment;
            }
            else if (this.keyboardManager.IsKeyDown(KeyData.KEY_LAYOUT_MANAGER_DOWN))
            {
                newSector.Height += increment;
            }
            if (this.keyboardManager.IsKeyDown(KeyData.KEY_LAYOUT_MANAGER_LEFT))
            {
                newSector.Width -= increment;
            }
            if (this.keyboardManager.IsKeyDown(KeyData.KEY_LAYOUT_MANAGER_RIGHT))
            {
                newSector.Width += increment;
            }
        }

        private void FineSize(int increment)
        {
            if (this.keyboardManager.IsFirstKeyPress(KeyData.KEY_LAYOUT_MANAGER_UP))
            {
                newSector.Height -= increment;
            }
            else if (this.keyboardManager.IsFirstKeyPress(KeyData.KEY_LAYOUT_MANAGER_DOWN))
            {
                newSector.Height += increment;
            }
            if (this.keyboardManager.IsFirstKeyPress(KeyData.KEY_LAYOUT_MANAGER_LEFT))
            {
                newSector.Width -= increment;
            }
            if (this.keyboardManager.IsFirstKeyPress(KeyData.KEY_LAYOUT_MANAGER_RIGHT))
            {
                newSector.Width += increment;
            }
        }
        
        public override void Draw(GameTime gameTime)
        {
            if ((isActive) && (!bPause))
            {
                //draw new proposed sector
                if (newSector != null)
                {
                    game.SpriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, SamplerState.LinearClamp,
                        DepthStencilState.Default, RasterizerState.CullNone, null, game.ActiveCamera.Transform.World);
                    game.SpriteBatch.Draw(texture, newSector.SectorBounds, null, newSector.Color, 0, Vector2.Zero, SpriteEffects.None, 0);

                    //draw rectangle
                    game.SpriteBatch.Draw(texture, newSector.SectorBounds, null, newSector.Color, 0, Vector2.Zero, SpriteEffects.None, 0);

                    game.SpriteBatch.End();
                }

                //draw existing sectors
                for (int i = 0; i < bspManager.Size(); i++)
                {
                    BSPSector sector = bspManager[i];
                    game.SpriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, SamplerState.LinearClamp,
                        DepthStencilState.Default, RasterizerState.CullNone, null, game.ActiveCamera.Transform.World);
                    //draw rectangle
                    game.SpriteBatch.Draw(texture, sector.SectorBounds, null, sector.Color, 0, Vector2.Zero, SpriteEffects.None, 0);

                    //draw sector number
                    Vector2 position = new Vector2(sector.SectorBounds.X + offset, sector.SectorBounds.Y + offset);
                    game.SpriteBatch.DrawString(font, sector.SectorNumber.ToString(), position, Color.White);
                    game.SpriteBatch.End();
                }
            }

            base.Draw(gameTime);
        }
        public override void Update(GameTime gameTime)
        {
            if ((isActive) && (!bPause))
            {
                CreateNewSector();
                UpdateSizePosition();
                AddNewSector();
                TestOverlap();
                TestDelete();
                TestEdit();
            }

            base.Update(gameTime);
        }
       
    }
}
