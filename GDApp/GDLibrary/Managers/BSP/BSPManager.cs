﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using System.IO;
using System;

namespace GDLibrary
{
    public class BSPManager : IService
    {
        private List<BSPSector> list;
        private string name;

        #region PROPERTIES
        public BSPSector this[int index]
        {
            get
            {
                return list[index];
            }
        }
        #endregion

        public BSPManager(string name)
        {
            this.name = name;
            Load();
        }

        public void Load()
        {
            List<BSPSector> tempList = SerializationUtility.Load<List<BSPSector>>(name, FileAccess.Read);
            if ((tempList == null) || (tempList.Count == 0))
            {
                this.list = new List<BSPSector>();
            }
            else
            {
                this.list = tempList;
            }

            SetCorrectBSPSectorNumbers();
        }

        private void SetCorrectBSPSectorNumbers()
        {
            for(int i = 0; i < this.list.Count; i++)
            {
                this.list[i].SectorNumber = (int)Math.Pow(2, i);
            }
            BSPSector.CurrentSectorIndex = (int)Math.Pow(2, this.list.Count-1);
        }
        public void Save()
        {
            SerializationUtility.Save<List<BSPSector>>(list, name, FileMode.Create);
        }

        public void Add(BSPSector s)
        {
            list.Add(s);
        }
        public bool Remove(BSPSector s)
        {
            return list.Remove(s);
        }
        public int Size()
        {
            return list.Count;
        }

        /// <summary>
        /// Returns the sector number for a sprite with the specified spritebounds rectangle
        /// </summary>
        /// <param name="spriteBounds"></param>
        /// <returns></returns>
        public int GetSectorNumber(Rectangle bounds)
        {
            int bSectorNumber = 0;

            foreach (BSPSector sector in list)
            {
                if (sector.SectorBounds.Intersects(bounds))
                {
                    bSectorNumber += sector.SectorNumber;
                }
            }
            return bSectorNumber;
        }

        public void SetSector(Transform2D transform)
        {
            //reset to zero
            transform.BSPSector = 0;

            foreach(BSPSector sector in list)
            {
                //if intersects this sector then perform bitwise OR with sector number
                if (sector.SectorBounds.Intersects(transform.Bounds))
                {
                    transform.BSPSector |= sector.SectorNumber;
                }
            }
        }
    }
}






