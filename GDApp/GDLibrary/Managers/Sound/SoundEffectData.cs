﻿using GDLibrary.Utility;
using Microsoft.Xna.Framework.Audio;
using GDApp;


namespace GDLibrary
{
    //See http://msdn.microsoft.com/en-us/library/bb195053.aspx
    public class SoundEffectData
    {
        #region Variables
        protected string name;
        protected string path;
        protected SoundEffectInstance soundEffectInstance;
        protected SoundEffect soundEffect;
        protected float volume, pitch, pan;
        protected bool loop;
        #endregion

        #region Properties
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        public float Volume
        {
            get
            {
                return volume;
            }
            set
            {
                volume = value;
            }
        }
        public float Pitch
        {
            get
            {
                return pitch;
            }
            set
            {
                pitch = value;
            }
        }
        public float Pan
        {
            get
            {
                return pan;
            }
            set
            {
                pan = value;
            }
        }
        public bool Loop
        {
            get
            {
                return loop;
            }
            set
            {
                loop = value;
            }
        }
        public SoundEffectInstance SoundEffectInstance
        {
            get
            {
                return soundEffectInstance;
            }
        }
        public SoundEffect SoundEffect
        {
            get
            {
                return soundEffect;
            }
        }
        #endregion

        public SoundEffectData(string path, float volume, float pitch, float pan, bool loop)
        {
            this.name = StringUtility.ParseNameFromPath(path).ToLower();
            this.path = path;

            this.volume = volume; 
            this.pitch = pitch; 
            this.pan = pan;
            this.loop = loop;
        }


        public void Load(Main game)
        {
            this.soundEffect = game.Content.Load<SoundEffect>(@"" + this.path);
        }

        public void Dispose()
        {
            this.soundEffect.Dispose();
            this.soundEffectInstance.Dispose();
        }
    }

}
