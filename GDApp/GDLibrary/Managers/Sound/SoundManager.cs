﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Audio;

using Microsoft.Xna.Framework;
using GDApp;

namespace GDLibrary
{
    public class SoundManager : GameComponent, IService
    {
        #region Variables
        private Main game;
        private Dictionary<string, SoundEffectData> dictionary;
        private HashSet<string> playSet;
        private List<SoundEffectInstance> playList;
        private int playDelayInMS;
        private int elapsedTime;

        //temp vars
        private SoundEffectData effectInfo;
        private SoundEffectInstance soundEffectInstance;
        #endregion

        #region Properties
        public int PlayDelayInMS
        {
            get
            {
                return playDelayInMS;
            }
            set
            {
                playDelayInMS = (value > 0) ? value : 0;
            }
        }
        #endregion

        public SoundEffectData this[string name]
        {
            get
            {
                name = name.ToLower();
                return this.dictionary[name];
            }
        }

        public SoundManager(Main game, int playDelayInMS)
            : base(game)
        {
            this.game = game;
            this.dictionary = new Dictionary<string, SoundEffectData>();
            this.playSet = new HashSet<string>();
            this.playList = new List<SoundEffectInstance>();

            PlayDelayInMS = playDelayInMS;
        }

        public bool Add(SoundEffectData effectInfo)
        {
            if (!dictionary.ContainsKey(effectInfo.Name))
            {
                effectInfo.Load(this.game);

                dictionary.Add(effectInfo.Name, effectInfo);
                return true;
            }

            return false;
        }

        public SoundEffectData GetEffectInfo(string name)
        {
            name = name.ToLower();
            if (dictionary.ContainsKey(name))
                return dictionary[name];
            else
                return null;
        }

        public SoundEffectInstance GetEffectInstance(string name)
        {
            name = name.ToLower();
            effectInfo = dictionary[name];
            soundEffectInstance = effectInfo.SoundEffect.CreateInstance();
            soundEffectInstance.Volume = effectInfo.Volume;
            soundEffectInstance.Pitch = effectInfo.Pitch;
            soundEffectInstance.Pan = effectInfo.Pan;
            soundEffectInstance.IsLooped = effectInfo.Loop;
            return soundEffectInstance;
        }

        public void Play(string name)
        {
            name = name.ToLower();

            if(!playSet.Contains(name)) //if we have not already been asked to play this in the current update loop then play it
            {
                SoundEffectData effectInfo = dictionary[name];
                SoundEffectInstance soundEffectInstance = effectInfo.SoundEffect.CreateInstance();
                soundEffectInstance.Volume = effectInfo.Volume;
                soundEffectInstance.Pitch = effectInfo.Pitch;
                soundEffectInstance.Pan = effectInfo.Pan;
                soundEffectInstance.IsLooped = effectInfo.Loop;

                playList.Add(soundEffectInstance);
                playSet.Add(name);
            }
        }


        public override void Update(GameTime gameTime)
        {
            elapsedTime += gameTime.ElapsedGameTime.Milliseconds;

            if(elapsedTime >= this.playDelayInMS)
            {
                for (int i = 0; i < this.playList.Count; i++)
                    playList[i].Play();

                elapsedTime = 0;
                playList.Clear();
                playSet.Clear();
            }
            base.Update(gameTime);
        }

        public bool Remove(string name)
        {
            name = name.ToLower();
            //find effect info so we can nullify for garbage collection
            SoundEffectData effectInfo = GetEffectInfo(name);

            //nullify for garbage collection
            effectInfo.Dispose();

            //remove the effect info from dictionary and store return value
            return dictionary.Remove(name);
        }

        public void Clear()
        {
            dictionary.Clear();
        }

        public int Size()
        {
            return dictionary.Count;
        }
    }

    
}
